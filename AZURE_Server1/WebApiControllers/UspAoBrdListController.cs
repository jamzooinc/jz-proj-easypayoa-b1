﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/UspAoBrdList




    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class UspAoBrdListDTI
    {
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
    }

    public class UspAoBrdListDTO
    {
        public UspAoBrdListDTO()
        {
            data = new List<Data>();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }

        public String ACCOUNT_STATUS_CODE;
        public String ACCOUNT_STATUS_MSG;

        public List<Data> data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String ERR_NO { get; set; }
            public String BD_MSG_NO { get; set; }
            public String BD_MSG_TEXT { get; set; }

        }
    }


    public class UspAoBrdListDTOo
    {
        public UspAoBrdListDTOo()
        {
            data = new List<Data>();
        }

        public String error_code { get; set; }
        public String error_msg { get; set; }


        //public String accountStatusCode;
        //public String accountStatusMsg;

        public String loginResult;
        public String errMsg;
        public String errNo;


        public List<Data> data;
        public class Data
        {
            public String bdMsgNo { get; set; }
            public String bdMsgText { get; set; }

        }
    }

    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================

    public class UspAoBrdListController : ApiController
    {


        // POST api/<controller>
        public UspAoBrdListDTOo Post(UspAoBrdListDTI uspAoBrdListDTI)
        {
            UspAoBrdListDTOo uspAoBrdListDTOo = new UspAoBrdListDTOo();

            try
            {
                string result = new HttpPostClient().exec(WebConfigurationManager.AppSettings["server2_ip"].ToString() + "api/USP_AO_BRD_LIST", uspAoBrdListDTI);

                var jsonFrom = new JavaScriptSerializer();
                UspAoBrdListDTO uspAoBrdListDTO = jsonFrom.Deserialize<UspAoBrdListDTO>(result);

                if (uspAoBrdListDTO.ERROR_CODE == "200")
                {
                    foreach(UspAoBrdListDTO.Data data in uspAoBrdListDTO.data)
                    {
                        UspAoBrdListDTOo.Data ddata = new UspAoBrdListDTOo.Data();

                        uspAoBrdListDTOo.loginResult = data.LOGIN_RESULT;
                        uspAoBrdListDTOo.errMsg = data.ERR_MSG;
                        uspAoBrdListDTOo.errNo = data.ERR_NO;

                        ddata.bdMsgNo = data.BD_MSG_NO;
                        ddata.bdMsgText = data.BD_MSG_TEXT;

                        uspAoBrdListDTOo.data.Add(ddata);
                    }
                }
                //---------------------------
                //      重包 Message
                //---------------------------
                if(uspAoBrdListDTOo.loginResult!= "Y")
                {
                    if (uspAoBrdListDTO.ACCOUNT_STATUS_CODE != "" && uspAoBrdListDTO.ACCOUNT_STATUS_MSG != "")
                    {
                        uspAoBrdListDTOo.errNo = uspAoBrdListDTO.ACCOUNT_STATUS_CODE;
                        uspAoBrdListDTOo.errMsg = uspAoBrdListDTO.ACCOUNT_STATUS_MSG;
                    }
                }

                //---------------------------
                //      Message
                //---------------------------
                uspAoBrdListDTOo.error_code = uspAoBrdListDTO.ERROR_CODE;
                uspAoBrdListDTOo.error_msg = uspAoBrdListDTO.ERROR_MSG;
            }
            catch (Exception ex)
            {
                uspAoBrdListDTOo.error_code = ErrorInfo.CATCH_CODE;
                uspAoBrdListDTOo.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }

            return uspAoBrdListDTOo;
        }

    }
}