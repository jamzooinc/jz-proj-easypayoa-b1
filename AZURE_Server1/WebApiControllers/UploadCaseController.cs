﻿using AZURE_Server1.DB;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;
using NLog;
using Newtonsoft.Json;

namespace AZURE_Server1.WebApiControllers
{

    public class UploadCaseDTI
    {
        public String MOBILE_NO { get; set; }
        public String ORI_CASE_NO { get; set; }
        public String APP_CASE_NO { get; set; }
        public int CASE_TYPE { get; set; }
        public String CUST_NAME { get; set; }
        public String APP_INFO { get; set; }
    }
    
    public class UploadCaseDTO
    {
        public String error_code { get; set; }
        public String error_msg { get; set; }
    }

    public class UploadCaseController : ApiController
    {
        private Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private void DownLoadZIP(UploadCaseDTI input, bool is_production, out string folderPath, out string schedulerFp)
        {
            #region 預備動作
            //office
            new FolderFunc(Constants.DISK_FOLDER_NAME_NEWCASE).CheckAndCreateFolder();
            new FolderFunc(Constants.DISK_FOLDER_NAME_OTHERCASE).CheckAndCreateFolder();
            new FolderFunc(Constants.DISK_FOLDER_NAME_PAYMENTCASE).CheckAndCreateFolder();
            
            //test
            new FolderFunc(Constants.DISK_FOLDER_NAME_NEWCASE_TEST).CheckAndCreateFolder();
            new FolderFunc(Constants.DISK_FOLDER_NAME_OTHERCASE_TEST).CheckAndCreateFolder();
            new FolderFunc(Constants.DISK_FOLDER_NAME_PAYMENTCASE_TEST).CheckAndCreateFolder();
            
            BlobMain blobMain = new BlobMain();

            blobMain.NotExistsCreate(Constants.CONTAINER_NAME_NEWCASE);
            blobMain.NotExistsCreate(Constants.CONTAINER_NAME_OTHERCASE);
            blobMain.NotExistsCreate(Constants.CONTAINER_NAME_PAYMENTCASE);

            //test
            blobMain.NotExistsCreate(Constants.CONTAINER_NAME_NEWCASE_TEST);
            blobMain.NotExistsCreate(Constants.CONTAINER_NAME_OTHERCASE_TEST);
            blobMain.NotExistsCreate(Constants.CONTAINER_NAME_PAYMENTCASE_TEST);

            folderPath = string.Empty;
            schedulerFp = string.Empty;

            #endregion

            //正式 dicList
            //D:\AE\AO_ZIP\bk-newcase
            //D:\AE\AO_ZIP\bk-othercase
            //D:\AE\AO_ZIP\bk-paymentcase

            //1進件 NEWCASE_TEST
            //2新建補件 NEWCASE_TEST
            //3覆核件 OTHERCASE_TEST
            //4撥款件 PAYMENTCASE_TEST

            var desFile = input.APP_CASE_NO + "_" + input.MOBILE_NO + ".zip";

            if (is_production)
            {
                switch (input.CASE_TYPE)
                {
                    case 3:
                        //NAME_OTHERCASE
                        blobMain.Download(desFile, Constants.CONTAINER_NAME_OTHERCASE, Constants.DISK_FOLDER_NAME_OTHERCASE);
                        folderPath = Constants.DISK_FOLDER_NAME_OTHERCASE;
                        schedulerFp = BlobConstants.DISK_FOLDER_NAME + "\\bk-" + Constants.CONTAINER_NAME_OTHERCASE;
                        break;
                    case 4:
                        //NAME_PAYMENTCASE
                        blobMain.Download(desFile, Constants.CONTAINER_NAME_PAYMENTCASE, Constants.DISK_FOLDER_NAME_PAYMENTCASE);
                        folderPath = Constants.DISK_FOLDER_NAME_PAYMENTCASE;
                        schedulerFp = BlobConstants.DISK_FOLDER_NAME + "\\bk-" + Constants.CONTAINER_NAME_PAYMENTCASE;
                        break;
                    default://1or2
                        //NAME_NEWCASE
                        blobMain.Download(desFile, Constants.CONTAINER_NAME_NEWCASE, Constants.DISK_FOLDER_NAME_NEWCASE);
                        folderPath = Constants.DISK_FOLDER_NAME_NEWCASE;
                        schedulerFp = BlobConstants.DISK_FOLDER_NAME + "\\bk-" + Constants.DISK_FOLDER_NAME_NEWCASE;
                        break;
                }
            }
            else //Test
            {
                switch (input.CASE_TYPE)
                {
                    case 3:
                        //NAME_OTHERCASE_TEST
                        blobMain.Download(desFile, Constants.CONTAINER_NAME_OTHERCASE_TEST, Constants.DISK_FOLDER_NAME_OTHERCASE_TEST);
                        folderPath = Constants.DISK_FOLDER_NAME_OTHERCASE_TEST;
                        schedulerFp = BlobConstants.DISK_FOLDER_NAME + "\\TEST_AO_ZIP\\bk-" + Constants.CONTAINER_NAME_OTHERCASE_TEST;
                        break;
                    case 4:
                        //NAME_PAYMENTCASE_TEST
                        blobMain.Download(desFile, Constants.CONTAINER_NAME_PAYMENTCASE_TEST, Constants.DISK_FOLDER_NAME_PAYMENTCASE_TEST);
                        folderPath = Constants.DISK_FOLDER_NAME_PAYMENTCASE_TEST;
                        schedulerFp = BlobConstants.DISK_FOLDER_NAME + "\\TEST_AO_ZIP\\bk-" + Constants.CONTAINER_NAME_PAYMENTCASE_TEST;
                        break;
                    default://1or2
                        //NAME_NEWCASE_TEST
                        blobMain.Download(desFile, Constants.CONTAINER_NAME_NEWCASE_TEST, Constants.DISK_FOLDER_NAME_NEWCASE_TEST);
                        //test oatestfile
                        //blobMain.Download(desFile, Constants.CONTAINER_NAME_OATESTFILE, Constants.DISK_FOLDER_NAME_NEWCASE_TEST);
                        folderPath = Constants.DISK_FOLDER_NAME_NEWCASE_TEST;
                        schedulerFp = BlobConstants.DISK_FOLDER_NAME + "\\TEST_AO_ZIP\\bk-" + Constants.CONTAINER_NAME_NEWCASE_TEST;
                        break;
                }
            }
            

        }
        /// <summary>
        /// 單筆案件上傳
        /// </summary>
        /// <param name="UploadCaseDTI"></param>
        /// <returns></returns>
        public UploadCaseDTO Post(UploadCaseDTI input)
        {
            UploadCaseDTO output = new UploadCaseDTO();
            if (string.IsNullOrEmpty(input.APP_CASE_NO) 
                || string.IsNullOrEmpty(input.APP_INFO)
                || input.CASE_TYPE == 0
                || string.IsNullOrEmpty(input.CUST_NAME)
                || string.IsNullOrEmpty(input.MOBILE_NO)
                || string.IsNullOrEmpty(input.ORI_CASE_NO))
            {
                output.error_code = "500";
                output.error_msg = "缺少必填資訊";
                return output;
            }

            try
            {
                logger.Info("UploadCase.Input=" + JsonConvert.SerializeObject(input));
                var is_production = Convert.ToBoolean(WebConfigurationManager.AppSettings["is_production"]);

                #region before
                ////單筆案件上傳 (DB), 看要不要順便傳app_info
                ////先下載ZIP
                //string folderPath = string.Empty;
                //string scheduleFolderPath = string.Empty;
                //DownLoadZIP(input, is_production, out folderPath , out scheduleFolderPath);

                ////正式
                ////解壓縮後圖片放置位置-> 之後再上傳blob
                ////D:\Easypayoa_S1_API\Test\CaseImage\newcase";
                ////D:\Easypayoa_S1_API\Test\CaseImage\othercase";
                ////D:\Easypayoa_S1_API\Test\CaseImage\paymentcase";

                ////測試
                ////解壓縮後圖片放置位置-> 之後再上傳blob
                ////D:\Easypayoa_S1_API\Release\CaseImage\test-othercase
                ////D:\Easypayoa_S1_API\Release\CaseImage\test-othercase
                ////D:\Easypayoa_S1_API\Release\CaseImage\test-paymentcase

                //var newAppInfo = string.Empty;
                ////1進件 2新建補件 3覆核件 4撥款件

                //if (Directory.Exists(folderPath) == false)
                //    Directory.CreateDirectory(folderPath);

                //List<string> delFolder = new List<string>();
                //var zipDir = string.Empty;

                //logger.Info("folderPath="+ folderPath);
                //logger.Info("scheduleFolderPath=" + scheduleFolderPath);
                //// 取得資料夾內所有檔案 
                //foreach (string fname in System.IO.Directory.GetFiles(folderPath))
                //{
                //    //檔名S2019090867945938826_0926639850
                //    var sourceFile = fname.Split('\\').LastOrDefault().ToUpper();
                //    var desFile = input.APP_CASE_NO + "_" + input.MOBILE_NO + ".ZIP";

                //    //找到檔案
                //    if (sourceFile == desFile.ToUpper() //檔名相符
                //        && sourceFile.Contains("ZIP")) //是ZIP檔案
                //    {
                //        logger.Info("blob find file . fname=" + fname);
                //        //建立資料夾
                //        zipDir = folderPath + "\\" + desFile.Replace(".ZIP", "");

                //        if (Directory.Exists(zipDir) == false)
                //            Directory.CreateDirectory(zipDir);

                //        logger.Info("blob zipDir=" + zipDir);

                //        logger.Info(string.Format("UploadImageToBlob({0},{1},{2})", fname.ToUpper(), zipDir, is_production));
                //        UploadImageToBlob(fname.ToUpper(), zipDir, is_production, out newAppInfo);

                //        logger.Info("blob newAppInfo=" + newAppInfo);
                //        //下載完後刪除ZIP檔案
                //        System.IO.File.Delete(fname.ToUpper());
                //    }
                //}

                ////代表正規地方沒找到,要到兩分鐘backup內找
                ////正式
                ////D:\AE\AO_ZIP\bk-newcase";
                ////D:\AE\AO_ZIP\bk-othercase";
                ////D:\AE\AO_ZIP\bk-paymentcase";
                ////測試
                ////D:\AE\AO_ZIP\TEST_AO_ZIP\bk-newcase";
                ////D:\AE\AO_ZIP\TEST_AO_ZIP\bk-othercase";
                ////D:\AE\AO_ZIP\TEST_AO_ZIP\bk-paymentcase";
                //if (string.IsNullOrEmpty(zipDir))
                //{
                //    logger.Info("schedule backup zipDir=" + zipDir);
                //    folderPath = scheduleFolderPath;
                //    foreach (string fname in System.IO.Directory.GetFiles(folderPath))
                //    {
                //        //檔名S2019090867945938826_0926639850
                //        var sourceFile = fname.Split('\\').LastOrDefault().ToUpper();
                //        var desFile = input.APP_CASE_NO + "_" + input.MOBILE_NO + ".ZIP".ToUpper();

                //        logger.Info("schedule backup sourceFile=" + sourceFile);
                //        //找到檔案
                //        if (sourceFile == desFile //檔名相符
                //            && sourceFile.Contains("ZIP")) //是ZIP檔案
                //        {
                //            logger.Info("schedule backup find file . fname=" + fname);
                //            //建立資料夾
                //            zipDir = folderPath + "\\" + desFile.Replace(".ZIP", "");

                //            if (Directory.Exists(zipDir) == false)
                //                Directory.CreateDirectory(zipDir);

                //            logger.Info("schedule backup zipDir=" + zipDir);

                //            UploadImageToBlob(fname.ToUpper(), zipDir, is_production, out newAppInfo);
                //            //delZipFile.Add(fname); 就是在備份檔拿得不用刪除了
                //            logger.Info("schedule backup newAppInfo=" + newAppInfo);
                //        }
                //    }
                //}

                ////解壓縮資料夾後搬移圖片檔案
                //delFolder.Add(zipDir);
                ////刪除解壓縮後的資料夾
                //foreach (string dir in delFolder)
                //{
                //    if (Directory.Exists(dir))
                //        System.IO.Directory.Delete(dir, true);
                //}

                #endregion
                //藉由ID去找儲存體解壓縮, 然後儲存資料
                using (var db = new EasyPayEntities())
                {
                    bool isSaveSuccess = false;
                    if (is_production)
                    {
                        using (var tran = db.Database.BeginTransaction())
                        {
                            if (db.Case.FirstOrDefault(x=>x.app_case_noe == input.APP_CASE_NO)==null)
                            {
                                db.Case.Add(new Case()
                                {
                                    app_case_noe = input.APP_CASE_NO,
                                    app_info = input.APP_INFO,
                                    case_type = input.CASE_TYPE,
                                    create_time = DateTime.Now,
                                    cust_name = input.CUST_NAME,
                                    ori_case_no = input.ORI_CASE_NO,
                                    img_root_url = "https://storageaefile.blob.core.windows.net/case-img/",
                                    mobile_no = input.MOBILE_NO
                                });
                                isSaveSuccess = db.SaveChanges() > 0;
                            }
                            tran.Commit();
                        }
                    }
                    else
                    {
                        using (var tran = db.Database.BeginTransaction())
                        {
                            if (db.CaseTest.FirstOrDefault(x => x.app_case_noe == input.APP_CASE_NO) == null)
                            {
                                db.CaseTest.Add(new CaseTest()
                                {
                                    app_case_noe = input.APP_CASE_NO,
                                    app_info = input.APP_INFO,
                                    case_type = input.CASE_TYPE,
                                    create_time = DateTime.Now,
                                    cust_name = input.CUST_NAME,
                                    ori_case_no = input.ORI_CASE_NO,
                                    img_root_url = "https://storageaefile.blob.core.windows.net/test-case-img/",
                                    mobile_no = input.MOBILE_NO
                                });
                                isSaveSuccess = db.SaveChanges() > 0;
                            }
                            tran.Commit();
                        }
                    }

                    if (isSaveSuccess)
                    {
                        output.error_code = "200";
                        output.error_msg = "成功";
                    }
                    else
                    {
                        output.error_code = "400";
                        output.error_msg = "上傳失敗";
                    }
                }
            }
            catch (Exception ex)
            {
                output.error_code = "900";
                output.error_msg = ex.ToString();
            }

            logger.Info("UploadCase.output=" + JsonConvert.SerializeObject(output));
            return output;
        }


        /// <summary>
        /// 將資料夾內圖片上傳到BLOB
        /// </summary>
        /// <param name="zipDir"></param>
        /// <param name="is_production"></param>
        /// <param name="appInfo"></param>
        private void UploadImageToBlob(string zipPath, string zipDir, bool is_production , out string appInfo)
        {

            using (var zip = ZipFile.Read(zipPath))//zip file
            {
                foreach (var zipEntry in zip)
                {
                    zipEntry.Extract(zipDir, ExtractExistingFileAction.OverwriteSilently);
                }
            }


            appInfo = string.Empty;
            foreach (string filePath in System.IO.Directory.GetFiles(zipDir))
            {
                var fn = filePath.Split('\\').LastOrDefault().ToUpper();
                if (fn.Contains(".JPG") ||
                    fn.Contains(".JPEG") ||
                    fn.Contains(".PNG") ||
                    fn.Contains(".BMP") ||
                    fn.Contains(".TIFF") ||
                    fn.Contains(".PCX") ||
                    fn.Contains(".TGA") ||
                    fn.Contains(".EXIF") ||
                    fn.Contains(".FPX") ||
                    fn.Contains(".SVG") ||
                    fn.Contains(".PSD") ||
                    fn.Contains(".CDR") ||
                    fn.Contains(".PCD") ||
                    fn.Contains(".DXF") ||
                    fn.Contains(".UFO") ||
                    fn.Contains(".EPS") ||
                    fn.Contains(".GIF"))
                {
                    #region 搬移圖片 -> 9/18改成上傳blob

                    //D:\Easypayoa_S1_API\Release
                    //D:\Easypayoa S1_API\Test
                    //string destFileName = "";
                    //if (is_production)
                    //{
                    //    destFileName = WebConfigurationManager.AppSettings["DISK_FOLDER_NAME_MAIN"];
                    //}
                    //else
                    //{
                    //    destFileName = WebConfigurationManager.AppSettings["DISK_FOLDER_NAME_MAIN_TEST"];
                    //}
                    //if (Directory.Exists(destFileName) == false) Directory.CreateDirectory(destFileName);
                    //File.Move(file, destFileName + fn);

                    #endregion

                    //上傳blob
                    BlobMain blobMain = new BlobMain();
                    var contrainerName = is_production
                        ? BlobConstants.CONTAINER_NAME_IMG
                        : BlobConstants.CONTAINER_NAME_IMG_TEST;

                    var url = blobMain.UploadAndBlobUrl(contrainerName, zipDir, fn);
                    logger.Info(string.Format("ZIP資料夾:{0}，下載下來的圖片檔案路徑{1}:", zipDir, url));
                }

                if (fn.Contains(".TXT"))
                {
                    //app_info
                    StreamReader str = new StreamReader(filePath);
                    var result = str.ReadToEnd();
                    if (!string.IsNullOrEmpty(result))
                    {
                        appInfo = result;
                    }

                    str.Close();
                }
            }

        }

    }
}