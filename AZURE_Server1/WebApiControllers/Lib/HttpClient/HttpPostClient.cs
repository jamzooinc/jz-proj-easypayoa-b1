﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace AZURE_Server1
{
    public class HttpPostClient
    {
        public string exec(string targetUrl, object dto)
        {
            HttpWebRequest request = HttpWebRequest.Create(targetUrl) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Timeout = 30000;

            //byte[] bytesData = Encoding.ASCII.GetBytes("1111");
            //using (Stream st = request.GetRequestStream())
            //{
            //    st.Write(bytesData, 0, bytesData.Length);
            //}

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(dto);
                streamWriter.Write(json);
                streamWriter.Flush();
            }

            string result = "";
            // 取得回應資料
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                }
            }
            return result;

        }
    }
}