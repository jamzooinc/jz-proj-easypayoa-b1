﻿using AZURE_Server1.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AZURE_Server1.WebApiControllers
{

    public class CaseListDTI
    {
        public String Ori_Case_No { get; set; }
        public int Case_Type { get; set; }
        public String MOBILE_NO { get; set; }
        /// <summary>
        /// yyyy/MM/dd
        /// </summary>
        public String Start_Date { get; set; }
        /// <summary>
        /// yyyy/MM/dd
        /// </summary>
        public String End_Date { get; set; }
        public int Page { get; set; }
        public CaseListDTI ()
        {
            Page = 1;
        }
    }

    public class CaseListDTO
    {
        public CaseListDTO()
        {
            data = new List<Data>();
        }
        public String error_code { get; set; }
        public String error_msg { get; set; }
        public List<Data> data { get; set; }
        public int current_page { get; set; }
        public int total_page { get; set; }
        public class Data
        {
            public string app_case_no { get; set; }
            public string ori_case_no { get; set; }
            public string cust_name { get; set; }
            public string app_info { get; set; }
            public string img_root_url { get; set; }
            public int case_type { get; set; }
            public string create_time { get; set; }
        }
    }

    public class CaseListController : ApiController
    {
        /// <summary>
        /// 單筆案件上傳
        /// </summary>
        /// <param name="CaseListDTI"></param>
        /// <returns></returns>
        public CaseListDTO Post(CaseListDTI input)
        {
            int pageSize = 20;
            CaseListDTO output = new CaseListDTO();
            if (string.IsNullOrEmpty(input.MOBILE_NO))
            {
                output.error_code = "500";
                output.error_msg = "缺少必填資訊";
                return output;
            }
            try
            {
                //案件清單列表查詢
                //可以直接撈DB
                using (var db = new EasyPayEntities())
                {
                    if (Convert.ToBoolean(WebConfigurationManager.AppSettings["is_production"]))
                    {
                        //正式
                        var list = db.Case.OrderByDescending(p => p.create_time).ToList();
                        if (!string.IsNullOrEmpty(input.MOBILE_NO))
                        {
                            list = list.Where(p => p.mobile_no == input.MOBILE_NO).ToList();
                        }
                        if (!string.IsNullOrEmpty(input.Start_Date))
                        {
                            DateTime sd = DateTime.Parse(input.Start_Date);
                            list = list.Where(p => p.create_time >= sd).ToList();
                        }
                        if (!string.IsNullOrEmpty(input.End_Date))
                        {
                            DateTime ed = DateTime.Parse(input.End_Date).AddDays(1);
                            list = list.Where(p => p.create_time < ed).ToList();
                        }

                        if (!string.IsNullOrEmpty(input.Ori_Case_No))
                        {
                            list = list.Where(p => p.ori_case_no == input.Ori_Case_No).ToList();
                        }

                        if (input.Case_Type != 0)
                        {
                            list = list.Where(p => p.case_type == input.Case_Type).ToList();
                        }

                        int j = list.Count;
                        int x = j / pageSize; //商數
                        int y = j - (x * pageSize); // 餘數

                        output.current_page = input.Page;
                        output.total_page = ((y != 0) ? x + 1 : x);

                        //做分頁
                        foreach (var i in list.Skip((input.Page - 1) * pageSize).Take(pageSize))
                        {
                            CaseListDTO.Data d = new CaseListDTO.Data();
                            d.app_case_no = i.app_case_noe;
                            d.app_info = i.app_info;
                            d.case_type = i.case_type;
                            d.create_time = i.create_time.ToString("yyyy/MM/dd HH:mm:ss");
                            d.cust_name = i.cust_name;
                            d.img_root_url = i.img_root_url;
                            d.ori_case_no = i.ori_case_no;
                            output.data.Add(d);
                        }
                    }
                    else
                    {
                        var list = db.CaseTest.OrderByDescending(p => p.create_time).ToList();
                        if (!string.IsNullOrEmpty(input.MOBILE_NO))
                        {
                            list = list.Where(p => p.mobile_no == input.MOBILE_NO).ToList();
                        }
                        if (!string.IsNullOrEmpty(input.Start_Date))
                        {
                            DateTime sd = DateTime.Parse(input.Start_Date);
                            list = list.Where(p => p.create_time >= sd).ToList();
                        }
                        if (!string.IsNullOrEmpty(input.End_Date))
                        {
                            DateTime ed = DateTime.Parse(input.End_Date).AddDays(1);
                            list = list.Where(p => p.create_time < ed).ToList();
                        }

                        if (!string.IsNullOrEmpty(input.Ori_Case_No))
                        {
                            list = list.Where(p => p.ori_case_no == input.Ori_Case_No).ToList();
                        }

                        if (input.Case_Type != 0)
                        {
                            list = list.Where(p => p.case_type == input.Case_Type).ToList();
                        }

                        int j = list.Count;
                        int x = j / pageSize; //商數
                        int y = j - (x * pageSize); // 餘數

                        output.current_page = input.Page;
                        output.total_page = ((y != 0) ? x + 1 : x);

                        //做分頁
                        foreach (var i in list.Skip((input.Page - 1) * pageSize).Take(pageSize))
                        {
                            CaseListDTO.Data d = new CaseListDTO.Data();
                            d.app_case_no = i.app_case_noe;
                            d.app_info = i.app_info;
                            d.case_type = i.case_type;
                            d.create_time = i.create_time.ToString("yyyy/MM/dd HH:mm:ss");
                            d.cust_name = i.cust_name;
                            d.img_root_url = i.img_root_url;
                            d.ori_case_no = i.ori_case_no;
                            output.data.Add(d);
                        }
                    }
                }
                output.error_code = "200";
                output.error_msg = "成功";
            }
            catch (Exception ex)
            {
                output.error_code = "900";
                output.error_msg = ex.ToString();
            }
            return output;
        }

    }
}