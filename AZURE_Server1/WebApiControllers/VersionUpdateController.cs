﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/VersionUpdate



    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class VersionUpdateDTI
    {
        public String OS_NAME { get; set; }
        public String MODE { get; set; }
    }

    public class MssqlAppVersionColumn
    {
        public String Id  { get; set; }
        public String OS_NAME { get; set; }
        public String MODE { get; set; }
        public String VERSION { get; set; }
        public String FORCE { get; set; }
    }


    public class VersionUpdateDTO
    {
        public VersionUpdateDTO()
        {
            data = new Data();
        }
        public String error_code { get; set; }
        public String error_msg { get; set; }
        public Data data;

        public class Data
        {
            public String version { get; set; }
            public String force { get; set; }
        }
    }

    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class VersionUpdateController : ApiController
    {

    
        // POST api/<controller>
        public VersionUpdateDTO Post(VersionUpdateDTI versionUpdateDTI)
        {
            return get(versionUpdateDTI);
        }

        private VersionUpdateDTO get(VersionUpdateDTI versionUpdateDTI)
        {

            VersionUpdateDTO versionUpdateDTO = new VersionUpdateDTO();
            try
            {
                string connectionString = WebConfigurationManager.AppSettings["AE_DB"].ToString();

                SqlConnection ConnMSSQL = new SqlConnection(connectionString);
                ConnMSSQL.Open();

                String testStoreProcedureStr = @"SELECT *
                                                    FROM[AE].[dbo].[APP_VERSION]
                                                    WHERE OS_NAME like @osName and MODE like @mode";

                SqlCommand cmd = new SqlCommand(testStoreProcedureStr, ConnMSSQL);

                cmd.Parameters.AddWithValue("@osName", versionUpdateDTI.OS_NAME);
                cmd.Parameters.AddWithValue("@mode", versionUpdateDTI.MODE);

                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        versionUpdateDTO.data.version = dr["VERSION"].ToString();
                        versionUpdateDTO.data.force = dr["FORCE"].ToString();
                    }

                    versionUpdateDTO.error_code = ErrorInfo.OK_CODE;
                    versionUpdateDTO.error_msg = ErrorInfo.OK_MSG;
                }
                else
                {
                    versionUpdateDTO.error_code = ErrorInfo.NO_DATA_CODE;
                    versionUpdateDTO.error_msg = ErrorInfo.NO_DATA_MSG;
                }

                //------------------
                //  關閉資料庫連線
                //------------------

                ConnMSSQL.Close();
                ConnMSSQL.Dispose();
            }
            catch (Exception ex)
            {
                versionUpdateDTO.error_code = ErrorInfo.CATCH_CODE;
                versionUpdateDTO.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }


            return versionUpdateDTO;
        }

    }
}