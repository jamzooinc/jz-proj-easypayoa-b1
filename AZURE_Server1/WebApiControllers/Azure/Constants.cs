﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Configuration;

namespace AZURE_Server1
{
    public class Constants
    {
        public const string STORAGE_CONNECTION_STRING = "DefaultEndpointsProtocol=https;AccountName=storageaefile;AccountKey=q4AeiiTZSIOdDJ0BlssKSneQyHyud3NkPMe5QGltlAIog6NfsDHPC9n4byONZ9WMONHehRByhUeBNKcA4Py26Q==;EndpointSuffix=core.windows.net";
        public const string BLOB_KEY = "q4AeiiTZSIOdDJ0BlssKSneQyHyud3NkPMe5QGltlAIog6NfsDHPC9n4byONZ9WMONHehRByhUeBNKcA4Py26Q==";

        //public const string CONTAINER_NAME = "test-blob-container";
        public const string CONTAINER_NAME_OATESTFILE = "oatestfile";

        public const string CONTAINER_NAME_NEWCASE = "newcase";
        public const string CONTAINER_NAME_OTHERCASE = "othercase";
        public const string CONTAINER_NAME_PAYMENTCASE = "paymentcase";

        public const string CONTAINER_NAME_NEWCASE_TEST = "test-newcase";
        public const string CONTAINER_NAME_OTHERCASE_TEST = "test-othercase";
        public const string CONTAINER_NAME_PAYMENTCASE_TEST = "test-paymentcase";

        //public const string DISK_FOLDER_NAME_MAIN = "E:\\asp_download\\AE\\AO_ZIP";
        //public const string DISK_FOLDER_NAME_MAIN_TEST = "E:\\asp_download\\AE\\TEST_AO_ZIP";
        

        public static string DISK_FOLDER_NAME_NEWCASE = WebConfigurationManager.AppSettings["DISK_FOLDER_NAME_MAIN"] + "\\newcase";
        public static string DISK_FOLDER_NAME_OTHERCASE = WebConfigurationManager.AppSettings["DISK_FOLDER_NAME_MAIN"] + "\\othercase";
        public static string DISK_FOLDER_NAME_PAYMENTCASE = WebConfigurationManager.AppSettings["DISK_FOLDER_NAME_MAIN"] + "\\paymentcase";

        //正式備份
        public static string DISK_FOLDER_NAME_NEWCASE_BACKUP = WebConfigurationManager.AppSettings["DISK_FOLDER_NAME_MAIN"] + "\\bk-newcase";
        public static string DISK_FOLDER_NAME_OTHERCASE_BACKUP = WebConfigurationManager.AppSettings["DISK_FOLDER_NAME_MAIN"] + "\\bk-othercase";
        public static string DISK_FOLDER_NAME_PAYMENTCASE_BACKUP = WebConfigurationManager.AppSettings["DISK_FOLDER_NAME_MAIN"] + "\\bk-paymentcase";

        public static string DISK_FOLDER_NAME_NEWCASE_TEST = WebConfigurationManager.AppSettings["DISK_FOLDER_NAME_MAIN_TEST"] + "\\test-newcase";
        public static string DISK_FOLDER_NAME_OTHERCASE_TEST = WebConfigurationManager.AppSettings["DISK_FOLDER_NAME_MAIN_TEST"] + "\\test-othercase";
        public static string DISK_FOLDER_NAME_PAYMENTCASE_TEST = WebConfigurationManager.AppSettings["DISK_FOLDER_NAME_MAIN_TEST"] + "\\test-paymentcase";

        //測試備份
        public static string DISK_FOLDER_NAME_NEWCASE_TEST_BACKUP = WebConfigurationManager.AppSettings["DISK_FOLDER_NAME_MAIN_TEST"] + "\\bk-test-newcase";
        public static string DISK_FOLDER_NAME_OTHERCASE_TEST_BACKUP = WebConfigurationManager.AppSettings["DISK_FOLDER_NAME_MAIN_TEST"] + "\\bk-test-othercase";
        public static string DISK_FOLDER_NAME_PAYMENTCASE_TEST_BACKUP = WebConfigurationManager.AppSettings["DISK_FOLDER_NAME_MAIN_TEST"] + "\\bk-test-paymentcase";


        //要存到Ｄ槽資料夾名稱AE 裡面一個叫AOZIP的資料夾
    }
}
