﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/UspAoCreditRpt


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class UspAoCreditRptDTI
    {
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String appCaseNo { get; set; }
        public String applNo { get; set; }
    }

    public class UspAoCreditRptDTO
    {
        public UspAoCreditRptDTO()
        {
            data = new List<Data>();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public List<Data> data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
        }
    }


    public class UspAoCreditRptDTOo
    {
        public UspAoCreditRptDTOo()
        {
            data = new Data();
        }

        public String error_code { get; set; }
        public String error_msg { get; set; }

        public Data data;

        public class Data
        {
            public String loginResult { get; set; }
           
        }
    }

    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class UspAoCreditRptController : ApiController
    {
        // POST api/<controller>
        public UspAoCreditRptDTOo Post(UspAoCreditRptDTI UspAoCreditRptDTI)
        {
            UspAoCreditRptDTOo UspAoCreditRptDTOo = new UspAoCreditRptDTOo();

            try
            {
                string result = new HttpPostClient().exec(WebConfigurationManager.AppSettings["server2_ip"].ToString() + "api/USP_AO_CREDIT_RPT", UspAoCreditRptDTI);

                var jsonFrom = new JavaScriptSerializer();
                UspAoCreditRptDTO UspAoCreditRptDTO = jsonFrom.Deserialize<UspAoCreditRptDTO>(result);
                //if (UspAoCreditRptDTO.ERROR_CODE == ErrorInfo.OK_CODE)
                //{
                UspAoCreditRptDTOo.data = new UspAoCreditRptDTOo.Data();
                foreach (UspAoCreditRptDTO.Data _data in UspAoCreditRptDTO.data)
                {
                    UspAoCreditRptDTOo.data.loginResult = _data.LOGIN_RESULT;
                }
                //}
                //---------------------------
                //      Message
                //---------------------------
                UspAoCreditRptDTOo.error_code = UspAoCreditRptDTO.ERROR_CODE;
                UspAoCreditRptDTOo.error_msg = UspAoCreditRptDTO.ERROR_MSG;
            }
            catch (Exception ex)
            {
                UspAoCreditRptDTOo.error_code = ErrorInfo.CATCH_CODE;
                UspAoCreditRptDTOo.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }

            return UspAoCreditRptDTOo;
        }

    }
}