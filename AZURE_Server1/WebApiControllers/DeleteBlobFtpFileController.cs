﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AZURE_Server1
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/DeleteBlobFtpFile



    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class DeleteBlobFtpFileDTI
    {
        public String filename { get; set; }
        public String key { get; set; }
    }

    public class DeleteBlobFtpFileDTO
    {
        public String CODE { get; set; }
        public String MSG { get; set; }
    }



    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class DeleteBlobFtpFileController : ApiController
    {
        // POST api/<controller>
        public DeleteBlobFtpFileDTO Post(DeleteBlobFtpFileDTI deleteBlobFtpFileDTI)
        {
            DeleteBlobFtpFileDTO deleteBlobFtpFileDTO = new DeleteBlobFtpFileDTO();

            deleteBlobFtpFileDTO.CODE = "401";
            deleteBlobFtpFileDTO.MSG = "已經取消這個功能";

#if false
            if (deleteBlobFtpFileDTI.key == BlobConstants.BLOB_KEY)
            {
                BlobMain blobMain = new BlobMain();

                //blobMain.Upload();
                //blobMain.Download();
                BlobMain.ResultStuts resultStuts = blobMain.Delete(deleteBlobFtpFileDTI.filename);

                deleteBlobFtpFileDTO.CODE = resultStuts.CODE;
                deleteBlobFtpFileDTO.MSG = resultStuts.MSG;
            }
            else
            {
                deleteBlobFtpFileDTO.CODE = "401";
                deleteBlobFtpFileDTO.MSG = "錯誤的KEY";
            }
#endif
            return deleteBlobFtpFileDTO;
        }
    }
}