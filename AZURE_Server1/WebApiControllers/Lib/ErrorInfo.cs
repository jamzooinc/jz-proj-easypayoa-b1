﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AZURE_Server1
{


    public class ErrorInfo
    { 
        public static string OK_CODE = "200";
        public static string OK_MSG = "成功";


        public static string CATCH_CODE = "001";
        public static string CATCH_MSG = "發生例外: ";

        public static string IDNG_CODE = "002";
        public static string IDNG_MSG = "帳號錯誤";

        public static string NO_DATA_CODE = "003";
        public static string NO_DATA_MSG = "查無資料, 或許輸入參數大小寫有錯.";
    }
}