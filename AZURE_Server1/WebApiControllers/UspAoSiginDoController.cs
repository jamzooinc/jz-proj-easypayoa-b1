﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/UspAoSiginDo


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class UspAoSiginDoDTI
    {
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String appCaseNo { get; set; }
        public String applNo { get; set; }
        public String siginTitle { get; set; }
        public String siginMemo { get; set; }
    }

    public class UspAoSiginDoDTO
    {
        public UspAoSiginDoDTO()
        {
            data = new List<Data>();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public List<Data> data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
        }
    }


    public class UspAoSiginDoDTOo
    {
        public UspAoSiginDoDTOo()
        {
            data = new Data();
        }

        public String error_code { get; set; }
        public String error_msg { get; set; }
        public Data data;

        public class Data
        {
            public String loginResult { get; set; }
            public String errMsg { get; set; }
            public String appCaseNo { get; set; }
            public String msgNo { get; set; }
            public String msgCount { get; set; }
            public String msgDate { get; set; }
        }
    }

    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class UspAoSiginDoController : ApiController
    {
        // POST api/<controller>
        public UspAoSiginDoDTOo Post(UspAoSiginDoDTI UspAoSiginDoDTI)
        {
            UspAoSiginDoDTOo UspAoSiginDoDTOo = new UspAoSiginDoDTOo();

            try
            {
                string result = new HttpPostClient().exec(WebConfigurationManager.AppSettings["server2_ip"].ToString() + "api/USP_AO_SIGIN_DO", UspAoSiginDoDTI);

                var jsonFrom = new JavaScriptSerializer();
                UspAoSiginDoDTO UspAoSiginDoDTO = jsonFrom.Deserialize<UspAoSiginDoDTO>(result);
                //if (UspAoSiginDoDTO.ERROR_CODE == ErrorInfo.OK_CODE)
                //{
                    UspAoSiginDoDTOo.data = new UspAoSiginDoDTOo.Data();
                    foreach (UspAoSiginDoDTO.Data _data in UspAoSiginDoDTO.data)
                    {
                        UspAoSiginDoDTOo.data.loginResult = _data.LOGIN_RESULT;
                    }
                //}
                //---------------------------
                //      Message
                //---------------------------
                UspAoSiginDoDTOo.error_code = UspAoSiginDoDTO.ERROR_CODE;
                UspAoSiginDoDTOo.error_msg = UspAoSiginDoDTO.ERROR_MSG;
            }
            catch (Exception ex)
            {
                UspAoSiginDoDTOo.error_code = ErrorInfo.CATCH_CODE;
                UspAoSiginDoDTOo.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }

            return UspAoSiginDoDTOo;
        }

    }
}