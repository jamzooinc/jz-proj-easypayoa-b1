﻿using AZURE_Server1.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/UspAoPayList

    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class UspAoPayListDTI
    {
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public int Page { get; set; }
    }

    public class UspAoPayListDTO
    {
        public UspAoPayListDTO()
        {
            data = new List<Data>();
        }
        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }

        public String ACCOUNT_STATUS_CODE;
        public String ACCOUNT_STATUS_MSG;

        public List<Data> data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String ERR_NO { get; set; }
            public String P_ID { get; set; }
            public String APPL_NO { get; set; }
            public String ENTER_DATE { get; set; }
            public String CH_NAME { get; set; }
            public String CASE_STATUS { get; set; }
            public String PROD_TYPE { get; set; }
            public String PROD_TP_NM { get; set; }
            public String BRAND_TYPE { get; set; }
            public String SUPPL_DATA { get; set; }
            public String MSG_CNT { get; set; }
        }
    }

    public class UspAoPayListDTOo
    {
        public UspAoPayListDTOo()
        {
            data = new List<Data>();
        }

        public String error_code { get; set; }
        public String error_msg { get; set; }

        public String loginResult;
        public String errMsg;
        public String errNo;

        public int current_page { get; set; }
        public int total_page { get; set; }

        public List<Data> data;

        public class Data
        {
            public String pID { get; set; }
            public String appNo { get; set; }
            public String enterDate { get; set; }
            public String chName { get; set; }
            public String caseStatus { get; set; }
            public String prodType { get; set; }
            public String prodTpNm { get; set; }
            public String brandType { get; set; }
            public String note { get; set; }
            public bool is_record { get; set; }
            public String msgCnt { get; set; }
            
        }
    }

    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class UspAoPayListController : ApiController
    {


        // POST api/<controller>
        public UspAoPayListDTOo Post(UspAoPayListDTI uspAoPayListDTI)
        {
            UspAoPayListDTOo uspAoPayListDTOo = new UspAoPayListDTOo();
            int pageSize = 20;
            try
            {
                string result = new HttpPostClient().exec(WebConfigurationManager.AppSettings["server2_ip"].ToString() + "api/USP_AO_PAY_LIST", uspAoPayListDTI);

                var jsonFrom = new JavaScriptSerializer();
                UspAoPayListDTO uspAoPayListDTO = jsonFrom.Deserialize<UspAoPayListDTO>(result);
                if (uspAoPayListDTO.ERROR_CODE == ErrorInfo.OK_CODE)
                {
                    var list = new List<UspAoPayListDTO.Data>();
                    if (uspAoPayListDTI.Page != 0)
                    {
                        int j = uspAoPayListDTO.data.Count;
                        int x = j / pageSize; //商數
                        int y = j - (x * pageSize); // 餘數

                        uspAoPayListDTOo.current_page = uspAoPayListDTI.Page;
                        uspAoPayListDTOo.total_page = ((y != 0) ? x + 1 : x);
                        list = uspAoPayListDTO.data.Skip((uspAoPayListDTI.Page - 1) * pageSize).Take(pageSize).ToList();
                    }
                    else
                    {
                        list = uspAoPayListDTO.data.ToList();
                    }

                    //做分頁
                    foreach (UspAoPayListDTO.Data _data in list)
                    {
                        UspAoPayListDTOo.Data data = new UspAoPayListDTOo.Data();

                        uspAoPayListDTOo.loginResult = _data.LOGIN_RESULT;
                        uspAoPayListDTOo.errMsg = _data.ERR_MSG;
                        uspAoPayListDTOo.errNo = _data.ERR_NO;

                        data.pID = _data.P_ID;
                        data.appNo = _data.APPL_NO;
                        data.enterDate = _data.ENTER_DATE;
                        data.chName = _data.CH_NAME;
                        data.caseStatus = _data.CASE_STATUS;
                        data.prodType = _data.PROD_TYPE;
                        data.prodTpNm = _data.PROD_TP_NM;
                        data.brandType = _data.BRAND_TYPE;
                        data.note = _data.SUPPL_DATA;
                        data.msgCnt = _data.MSG_CNT;
                        //data.is_record = 
                        var is_production = Convert.ToBoolean(WebConfigurationManager.AppSettings["is_production"]);

                        data.is_record = false;
                        using (var db = new EasyPayEntities())
                        {
                            var sd = DateTime.Now.AddDays(-3).Date;
                            var ed = DateTime.Now.AddDays(1).Date;
                            if (is_production)
                            {
                                if (db.Case.Any(p => p.case_type == 5 
                                && p.ori_case_no == _data.APPL_NO
                                && p.create_time >= sd
                                && p.create_time < ed))
                                {
                                    data.is_record = true;
                                }
                            }
                            else
                            {
                                if (db.CaseTest.Any(p => p.case_type == 5
                                && p.ori_case_no == _data.APPL_NO
                                && p.create_time >= sd
                                && p.create_time < ed))
                                {
                                    data.is_record = true;
                                }
                            }
                        }
                        uspAoPayListDTOo.data.Add(data);
                    }

                }

                //---------------------------
                //      重包 Message
                //---------------------------
                if (uspAoPayListDTOo.loginResult != "Y")
                {
                    if(uspAoPayListDTO.ACCOUNT_STATUS_CODE!="" && uspAoPayListDTO.ACCOUNT_STATUS_MSG!="")
                    {
                        uspAoPayListDTOo.errNo = uspAoPayListDTO.ACCOUNT_STATUS_CODE;
                        uspAoPayListDTOo.errMsg = uspAoPayListDTO.ACCOUNT_STATUS_MSG;
                    }
                }

                //---------------------------
                //      Message
                //---------------------------
                uspAoPayListDTOo.error_code = uspAoPayListDTO.ERROR_CODE;
                uspAoPayListDTOo.error_msg = uspAoPayListDTO.ERROR_MSG;
            }
            catch (Exception ex)
            {
                uspAoPayListDTOo.error_code = ErrorInfo.CATCH_CODE;
                uspAoPayListDTOo.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }

            return uspAoPayListDTOo;
        }

    }
}