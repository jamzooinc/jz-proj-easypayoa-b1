﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AZURE_Server1.WebApiControllers
{   
    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================
    public class UspAoAppUpdateListController : ApiController
    {
        //*********************************************************************
        //*********************************************************************
        //      ip
        //=====================================================================
        //http://localhost:55015/api/UspAoAppUpdate


        //*********************************************************************
        //*********************************************************************
        //      DTI(data to in)   and   DTO(data to out)   class
        //=====================================================================
        public class UspAoAppUpdateDTI
        {
            public String MOBILE_NO { get; set; }
            public String MOBILE_ID { get; set; }
            public String APP_CASE_NO { get; set; }
        }

        public class UspAoAppUpdateDTO
        {
            public UspAoAppUpdateDTO()
            {
                data = new List<Data>();
            }

            public String ERROR_CODE { get; set; }
            public String ERROR_MSG { get; set; }
            public List<Data> data;

            public class Data
            {
                public String LOGIN_RESULT { get; set; }
                public String ERR_MSG { get; set; }
                public String ERR_NO { get; set; }
                public String APP_CASE_NO { get; set; }
                public String APPL_NO { get; set; }
                public String P_ID { get; set; }
                public String CH_NAME { get; set; }
                public String CASE_STATUS { get; set; }
                public String UPDATE_FLG { get; set; }
                //public String APP_FORM_2 { get; set; }//
                public String PROD_TYPE { get; set; }
                public String PROD_TYPE_NM { get; set; }
                public String FUND_SRC { get; set; }
                public String FUND_SRC_NM { get; set; }
                public String BANK_NA { get; set; }
                public String BANK_NA_NM { get; set; }
                //public String NOQRY_FLG { get; set; }//
                public String PROJ_ID { get; set; }
                public String PROJ_ID_NM { get; set; }
                public String DLR_NO { get; set; }
                public String DLR_NO_NM { get; set; }
                public String BRNH_NO { get; set; }
                public String BRNH_NO_NM { get; set; }
                public String SALES_NO { get; set; }
                public String SALES_NO_NM { get; set; }
                public String MSG_CNT { get; set; }
                public String SEQ_NO { get; set; }
                public String BACK_MENT { get; set; }
                public String CREDIT_RPT { get; set; }
                public String SIGIN_FLG { get; set; }
            }
        }
        
        public class UspAoAppUpdateDTOo
        {
            public UspAoAppUpdateDTOo()
            {
                data = new List<Data>();
            }

            public String error_code { get; set; }
            public String error_msg { get; set; }
            public List<Data> data;

            public class Data
            {
                public String loginResult { get; set; }
                public String errMsg { get; set; }
                public String errNo { get; set; }
                public String appCaseNo { get; set; }
                public String appNo { get; set; }
                public String pID { get; set; }
                public String chName { get; set; }
                public String caseStatus { get; set; }
                public String updateFlg { get; set; }
                //public String appForm2 { get; set; }  //
                public String prosType { get; set; }
                public String prodTypeNm { get; set; }
                public String fundSrc { get; set; }
                public String fundSrcNm { get; set; }
                public String bankNa { get; set; }
                public String bankNaNm { get; set; }
                //public String NoqryFlg { get; set; }//
                public String projID { get; set; }
                public String projIdNm { get; set; }
                public String dlrNo { get; set; }
                public String dlrNoNm { get; set; }
                public String brnhNo { get; set; }
                public String brohNoNm { get; set; }
                public String salesNo { get; set; }
                public String salesNoNm { get; set; }
                public String msgCnt { get; set; }
                public String seqNo { get; set; }
                public String backMent { get; set; }
                public String creditRptFlg { get; set; }
                public String siginFlg { get; set; }
            }
        }

        // POST api/<controller>
        public UspAoAppUpdateDTOo Post(UspAoAppUpdateDTI uspAoAppUpdateDTI)
        {
            Logger logger = NLog.LogManager.GetCurrentClassLogger();
            UspAoAppUpdateDTOo uspAoAppUpdateDTOo = new UspAoAppUpdateDTOo();

            try
            {
                string result = new HttpPostClient().exec(WebConfigurationManager.AppSettings["server2_ip"].ToString() + "api/USP_AO_APP_UPDATE_LIST", uspAoAppUpdateDTI);

                var jsonFrom = new JavaScriptSerializer();
                UspAoAppUpdateDTO uspAoAppUpdateDTO = jsonFrom.Deserialize<UspAoAppUpdateDTO>(result);
                if (uspAoAppUpdateDTO.ERROR_CODE == "200")
                {
                    foreach (var item in uspAoAppUpdateDTO.data)
                    {
                        var d = new UspAoAppUpdateDTOo.Data();

                        d.loginResult = item.LOGIN_RESULT;
                        d.errMsg = item.ERR_MSG;
                        d.errNo = item.ERR_NO;
                        d.appCaseNo = item.APP_CASE_NO;
                        d.appNo = item.APPL_NO;
                        d.pID = item.P_ID;
                        d.chName = item.CH_NAME;
                        d.caseStatus = item.CASE_STATUS;
                        d.updateFlg = item.UPDATE_FLG;
                        //d.appForm2 = item.APP_FORM_2;//
                        d.prosType = item.PROD_TYPE;
                        d.prodTypeNm = item.PROD_TYPE_NM;
                        d.fundSrc = item.FUND_SRC;
                        d.fundSrcNm = item.FUND_SRC_NM;
                        d.bankNa = item.BANK_NA;
                        d.bankNaNm = item.BANK_NA_NM;
                        //d.NoqryFlg = item.NOQRY_FLG;//
                        d.projID = item.PROJ_ID;
                        d.projIdNm = item.PROJ_ID_NM;
                        d.dlrNo = item.DLR_NO;
                        d.dlrNoNm = item.DLR_NO_NM;
                        d.brnhNo = item.BRNH_NO;
                        d.brohNoNm = item.BRNH_NO_NM;
                        d.salesNo = item.SALES_NO;
                        d.salesNoNm = item.SALES_NO_NM;
                        d.msgCnt = item.MSG_CNT;
                        d.seqNo = item.SEQ_NO;
                        d.backMent = item.BACK_MENT;
                        d.creditRptFlg = item.CREDIT_RPT;
                        d.siginFlg = item.SIGIN_FLG;
                        uspAoAppUpdateDTOo.data.Add(d);
                    }

                    foreach(string caseNo in uspAoAppUpdateDTI.APP_CASE_NO.Split(','))
                    {
                        if (!uspAoAppUpdateDTOo.data.Where(p=>p.appCaseNo.Equals(caseNo)).Any())
                        {
                            uspAoAppUpdateDTOo.data.Add(new UspAoAppUpdateDTOo.Data()
                            {
                                appCaseNo = caseNo
                            });
                        }
                    }
                }
                //---------------------------
                //      Message
                //---------------------------
                uspAoAppUpdateDTOo.error_code = uspAoAppUpdateDTO.ERROR_CODE;
                uspAoAppUpdateDTOo.error_msg = uspAoAppUpdateDTO.ERROR_MSG;
            }
            catch (Exception ex)
            {
                uspAoAppUpdateDTOo.error_code = ErrorInfo.CATCH_CODE;
                uspAoAppUpdateDTOo.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }
            return uspAoAppUpdateDTOo;
        }


    }
}