﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/DlrNo



    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class DlrNoDTI : LoginDTI
    {
        /*
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String MOBILE_TYPE { get; set; }
        public String OS_NAME { get; set; }
        public String OS_VERNO { get; set; }
        */

        public String dlrNo { get; set; }
        public String dlrNoNm { get; set; }
    }

    public class DlrNoDTO
    {
        public DlrNoDTO()
        {
            data = new List<Data>();
        }
        public String error_code { get; set; }
        public String error_msg { get; set; }
        public List<Data> data;
        public class Data
        {
            public String dlrNo { get; set; }
            public String dlrNoNm { get; set; }
            public String philoNo { get; set; }
        }
    }


    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class DlrNoController : ApiController
    {


        // POST api/<controller>
        public DlrNoDTO Post(DlrNoDTI dlrNoDTI)
        {
            LoginDTOo loginDTOo = new LoginController().Post(dlrNoDTI);

            DlrNoDTO dlrNoDTO = new DlrNoDTO();
            if (loginDTOo.data.loginResult == "Y")
            {
                dlrNoDTO = get(dlrNoDTI);
                dlrNoDTO.error_code = ErrorInfo.OK_CODE;
                dlrNoDTO.error_msg = ErrorInfo.OK_MSG;
            }
            else
            {
                dlrNoDTO.error_code = ErrorInfo.IDNG_CODE;
                dlrNoDTO.error_msg = ErrorInfo.IDNG_MSG;
            }
            return dlrNoDTO;
        }

        private DlrNoDTO get(DlrNoDTI dlrNoDTI)
        {
            DlrNoDTO dlrNoDTO = new DlrNoDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["AE_DB"].ToString();

                SqlConnection ConnMSSQL = new SqlConnection(connectionString);
                ConnMSSQL.Open();

                String testStoreProcedureStr = @"SELECT * FROM AE_DLR_NO";
                if (!string.IsNullOrEmpty(dlrNoDTI.dlrNo))
                {
                    testStoreProcedureStr += " WHERE DLR_NO LIKE N'%"+ dlrNoDTI.dlrNo + "%'";
                }

                if (!string.IsNullOrEmpty(dlrNoDTI.dlrNoNm))
                {
                    if (!testStoreProcedureStr.Contains("WHERE"))
                    {
                        testStoreProcedureStr += " WHERE DLR_NO_NM LIKE '%" + dlrNoDTI.dlrNoNm + "%'";
                    }
                    else
                    {
                        testStoreProcedureStr += " AND DLR_NO_NM LIKE '%" + dlrNoDTI.dlrNoNm + "%'";
                    }
                }
                testStoreProcedureStr += ";";

                SqlCommand cmd = new SqlCommand(testStoreProcedureStr, ConnMSSQL);
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        DlrNoDTO.Data data = new DlrNoDTO.Data();
                        data.dlrNo = dr["DLR_NO"].ToString();
                        data.dlrNoNm = dr["DLR_NO_NM"].ToString();
                        data.philoNo = dr["PHILO_NO"].ToString();
                        dlrNoDTO.data.Add(data);
                    }
                }

                //------------------
                //  關閉資料庫連線
                //------------------

                ConnMSSQL.Close();
                ConnMSSQL.Dispose();

                dlrNoDTO.error_code = ErrorInfo.OK_CODE;
                dlrNoDTO.error_msg = ErrorInfo.OK_MSG;
            }
            catch (Exception ex)
            {
                dlrNoDTO.error_code = ErrorInfo.CATCH_CODE;
                dlrNoDTO.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }



            return dlrNoDTO;

        }
    }
}