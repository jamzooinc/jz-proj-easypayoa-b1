﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/UspAoAppMsg


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class UspAoAppMsgDTI
    {
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String APP_CASE_NO { get; set; }
    }

    public class UspAoAppMsgDTO
    {
        public UspAoAppMsgDTO()
        {
            data = new List<Data>();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public List<Data> data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String APP_CASE_NO { get; set; }
            public String MSG_NO { get; set; }
            public String MSG_CONTENT { get; set; }
            public String MSG_DATE { get; set; }
        }
    }


    public class UspAoAppMsgDTOo
    {
        public UspAoAppMsgDTOo()
        {
            data = new List<Data>();
        }

        public String error_code { get; set; }
        public String error_msg { get; set; }
        public List<Data> data;

        public class Data
        {
            public String loginResult { get; set; }
            public String errMsg { get; set; }
            public String appCaseNo { get; set; }
            public String msgNo { get; set; }
            public String msgCount { get; set; }
            public String msgDate { get; set; }
        }
    }

    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class UspAoAppMsgController : ApiController
    {
        // POST api/<controller>
        public UspAoAppMsgDTOo Post(UspAoAppMsgDTI uspAoAppMsgDTI)
        {
            UspAoAppMsgDTOo uspAoAppMsgDTOo = new UspAoAppMsgDTOo();

            try
            {
                string result = new HttpPostClient().exec(WebConfigurationManager.AppSettings["server2_ip"].ToString() + "api/USP_AO_APP_MSG", uspAoAppMsgDTI);

                var jsonFrom = new JavaScriptSerializer();
                UspAoAppMsgDTO uspAoAppMsgDTO = jsonFrom.Deserialize<UspAoAppMsgDTO>(result);
                if (uspAoAppMsgDTO.ERROR_CODE == ErrorInfo.OK_CODE)
                {
                    foreach (UspAoAppMsgDTO.Data _data in uspAoAppMsgDTO.data)
                    {
                        UspAoAppMsgDTOo.Data data = new UspAoAppMsgDTOo.Data();

                        data.loginResult = _data.LOGIN_RESULT;
                        data.errMsg = _data.ERR_MSG;
                        data.appCaseNo = _data.APP_CASE_NO;
                        data.msgNo = _data.MSG_NO;
                        data.msgCount = _data.MSG_CONTENT;
                        data.msgDate = _data.MSG_DATE;
                    
                        uspAoAppMsgDTOo.data.Add(data);
                    }
                }
                //---------------------------
                //      Message
                //---------------------------
                uspAoAppMsgDTOo.error_code = uspAoAppMsgDTO.ERROR_CODE;
                uspAoAppMsgDTOo.error_msg = uspAoAppMsgDTO.ERROR_MSG;
            }
            catch (Exception ex)
            {
                uspAoAppMsgDTOo.error_code = ErrorInfo.CATCH_CODE;
                uspAoAppMsgDTOo.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }

            return uspAoAppMsgDTOo;
        }

    }
}