﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/BankNa



    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class BankNaDTI : LoginDTI
    {
        /*
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String MOBILE_TYPE { get; set; }
        public String OS_NAME { get; set; }
        public String OS_VERNO { get; set; }
        */
    }

    public class BankNaDTO
    {
        public BankNaDTO()
        {
            data = new List<Data>();
        }
        public String error_code { get; set; }
        public String error_msg { get; set; }
        public List<Data> data;
        public class Data
        {
            public String bankNa { get; set; }
            public String bankNaNm { get; set; }
        }
    }


    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class BankNaController : ApiController
    {

        // POST api/<controller>
        public BankNaDTO Post(BankNaDTI bankNaDTI)
        {
            LoginDTOo loginDTOo = new LoginController().Post(bankNaDTI);

            BankNaDTO bankNaDTO = new BankNaDTO();
            if (loginDTOo.data.loginResult == "Y")
            {
                bankNaDTO = get();
                bankNaDTO.error_code = ErrorInfo.OK_CODE;
                bankNaDTO.error_msg = ErrorInfo.OK_MSG;
            }
            else
            {
                bankNaDTO.error_code = ErrorInfo.IDNG_CODE;
                bankNaDTO.error_msg = ErrorInfo.IDNG_MSG;
            }
            return bankNaDTO;
        }

        private BankNaDTO get()
        {
            BankNaDTO bankNaDTO = new BankNaDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["AE_DB"].ToString();

                SqlConnection ConnMSSQL = new SqlConnection(connectionString);
                ConnMSSQL.Open();

                String testStoreProcedureStr = @"SELECT * FROM AE_BANK_NA;";
                SqlCommand cmd = new SqlCommand(testStoreProcedureStr, ConnMSSQL);
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        BankNaDTO.Data data = new BankNaDTO.Data();
                        data.bankNa = dr["BANK_NA"].ToString();
                        data.bankNaNm = dr["BANK_NA_NM"].ToString();
                        bankNaDTO.data.Add(data);
                    }
                }

                //------------------
                //  關閉資料庫連線
                //------------------

                ConnMSSQL.Close();
                ConnMSSQL.Dispose();

                bankNaDTO.error_code = ErrorInfo.OK_CODE;
                bankNaDTO.error_msg = ErrorInfo.OK_MSG;
            }
            catch (Exception ex)
            {
                bankNaDTO.error_code = ErrorInfo.CATCH_CODE;
                bankNaDTO.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }



            return bankNaDTO;

        }
    }
}