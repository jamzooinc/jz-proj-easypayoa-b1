﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/BamspecM



    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class BamspecMDTI : LoginDTI
    {
        /*
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String MOBILE_TYPE { get; set; }
        public String OS_NAME { get; set; }
        public String OS_VERNO { get; set; }
        */
    }

    public class BamspecMDTO
    {
        public BamspecMDTO()
        {
            data = new List<Data>();
        }
        public String error_code { get; set; }
        public String error_msg { get; set; }
        public List<Data> data;
        public class Data
        {
            public String projNo { get; set; }
            public String projNoNm { get; set; }
        }
    }


    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class BamspecMController : ApiController
    {

        // POST api/<controller>
        public BamspecMDTO Post(BamspecMDTI bamspecMDTI)
        {
            LoginDTOo loginDTOo = new LoginController().Post(bamspecMDTI);

            BamspecMDTO bamspecMDTO = new BamspecMDTO();
            if (loginDTOo.data.loginResult == "Y")
            {
                bamspecMDTO = get();
                bamspecMDTO.error_code = ErrorInfo.OK_CODE;
                bamspecMDTO.error_msg = ErrorInfo.OK_MSG;
            }
            else
            {
                bamspecMDTO.error_code = ErrorInfo.IDNG_CODE;
                bamspecMDTO.error_msg = ErrorInfo.IDNG_MSG;
            }
            return bamspecMDTO;
        }

        private BamspecMDTO get()
        {
            BamspecMDTO bamspecMDTO = new BamspecMDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["AE_DB"].ToString();

                SqlConnection ConnMSSQL = new SqlConnection(connectionString);
                ConnMSSQL.Open();

                String testStoreProcedureStr = @"SELECT * FROM AE_BAMSPEC_M;";
                SqlCommand cmd = new SqlCommand(testStoreProcedureStr, ConnMSSQL);
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        BamspecMDTO.Data data = new BamspecMDTO.Data();
                        data.projNo = dr["SPEC_NO"].ToString();
                        data.projNoNm = dr["SPEC_NO_NM"].ToString();
                        bamspecMDTO.data.Add(data);
                    }
                }

                //------------------
                //  關閉資料庫連線
                //------------------

                ConnMSSQL.Close();
                ConnMSSQL.Dispose();

                bamspecMDTO.error_code = ErrorInfo.OK_CODE;
                bamspecMDTO.error_msg = ErrorInfo.OK_MSG;
            }
            catch (Exception ex)
            {
                bamspecMDTO.error_code = ErrorInfo.CATCH_CODE;
                bamspecMDTO.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }



            return bamspecMDTO;

        }

    }
}