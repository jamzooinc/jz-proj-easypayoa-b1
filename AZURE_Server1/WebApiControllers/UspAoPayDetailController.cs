﻿using AZURE_Server1.DB;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AZURE_Server1.WebApiControllers
{

    public class UspAoPayDetailDTI
    {
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        /// <summary>
        /// yyyy/MM/dd
        /// </summary>
        public String appNo { get; set; }
        public int caseType { get; set; }

        public UspAoPayDetailDTI()
        {
            caseType = 5;
        }
    }

    public class UspAoPayDetailDTO
    {
        public UspAoPayDetailDTO()
        {
            data = new Data();
        }
        public String error_code { get; set; }
        public String error_msg { get; set; }
        public Data data { get; set; }
        public class Data
        {
            public string app_case_no { get; set; }
            public string ori_case_no { get; set; }
            public string cust_name { get; set; }
            public string app_info { get; set; }
            public string img_root_url { get; set; }
            public int case_type { get; set; }
            public string create_time { get; set; }
        }
    }

    public class UspAoPayDetailController : ApiController
    {
        /// <summary>
        /// 單筆案件上傳
        /// </summary>
        /// <param name="UspAoPayDetailDTI"></param>
        /// <returns></returns>
        public UspAoPayDetailDTO Post(UspAoPayDetailDTI input)
        {
            UspAoPayDetailDTO output = new UspAoPayDetailDTO();
            if (string.IsNullOrEmpty(input.MOBILE_NO))
            {
                output.error_code = "500";
                output.error_msg = "缺少必填資訊";
                return output;
            }
            try
            {
                //案件清單列表查詢
                //可以直接撈DB
                using (var db = new EasyPayEntities())
                {
                    if (Convert.ToBoolean(WebConfigurationManager.AppSettings["is_production"]))
                    {
                        //正式
                        //案件類型 
                        //1 - 進件 2 - 新件補件 3 - 覆核件 5 - 撥款件
                        var i = db.Case.FirstOrDefault(p => p.case_type == input.caseType
                                                                && p.mobile_no == input.MOBILE_NO
                                                                && p.app_case_noe == input.appNo);

                        output.data = new UspAoPayDetailDTO.Data();
                        if (i != null)
                        {
                            output.data.app_case_no = i.app_case_noe;
                            output.data.app_info = i.app_info;
                            output.data.case_type = i.case_type;
                            output.data.create_time = i.create_time.ToString("yyyy/MM/dd HH:mm:ss");
                            output.data.cust_name = i.cust_name;
                            output.data.img_root_url = i.img_root_url;
                            output.data.ori_case_no = i.ori_case_no;
                        }
                    }
                    else
                    {
                        //測試
                        //案件類型 
                        //1 - 進件 2 - 新件補件 3 - 覆核件 5 - 撥款件
                        var i = db.CaseTest.FirstOrDefault(p => p.case_type == input.caseType
                                                                && p.mobile_no == input.MOBILE_NO
                                                                && p.app_case_noe == input.appNo);

                        output.data = new UspAoPayDetailDTO.Data();
                        if (i!=null)
                        {
                            output.data.app_case_no = i.app_case_noe;
                            output.data.app_info = i.app_info;
                            output.data.case_type = i.case_type;
                            output.data.create_time = i.create_time.ToString("yyyy/MM/dd HH:mm:ss");
                            output.data.cust_name = i.cust_name;
                            output.data.img_root_url = i.img_root_url;
                            output.data.ori_case_no = i.ori_case_no;
                        }
                    }
                }
                output.error_code = "200";
                output.error_msg = "成功";
            }
            catch (Exception ex)
            {
                output.error_code = "900";
                output.error_msg = ex.ToString();
            }
            return output;
        }

    }
}