﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/FundSrc



    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class FundSrcDTI : LoginDTI
    {
        /*
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String MOBILE_TYPE { get; set; }
        public String OS_NAME { get; set; }
        public String OS_VERNO { get; set; }
        */
    }

    public class FundSrcDTO
    {
        public FundSrcDTO()
        {
            data = new List<Data>();
        }
        public String error_code { get; set; }
        public String error_msg { get; set; }
        public List<Data> data;
        public class Data
        {
            public String fundSrc { get; set; }
            public String fundSrcNm { get; set; }
        }
    }


    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class FundSrcController : ApiController
    {
        // POST api/<controller>
        public FundSrcDTO Post(FundSrcDTI fundSrcDTI)
        {
            LoginDTOo loginDTOo = new LoginController().Post(fundSrcDTI);

            FundSrcDTO fundSrcDTO = new FundSrcDTO();
            if (loginDTOo.data.loginResult == "Y")
            {
                fundSrcDTO = get();
                fundSrcDTO.error_code = ErrorInfo.OK_CODE;
                fundSrcDTO.error_msg = ErrorInfo.OK_MSG;
            }
            else
            {
                fundSrcDTO.error_code = ErrorInfo.IDNG_CODE;
                fundSrcDTO.error_msg = ErrorInfo.IDNG_MSG;
            }
            return fundSrcDTO;
        }



        private FundSrcDTO get()
        {
            FundSrcDTO fundSrcDTO = new FundSrcDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["AE_DB"].ToString();

                SqlConnection ConnMSSQL = new SqlConnection(connectionString);
                ConnMSSQL.Open();

                String testStoreProcedureStr = @"SELECT * FROM AE_FUND_SRC;";
                SqlCommand cmd = new SqlCommand(testStoreProcedureStr, ConnMSSQL);
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        FundSrcDTO.Data data = new FundSrcDTO.Data();
                        data.fundSrc = dr["FUND_SRC"].ToString();
                        data.fundSrcNm = dr["FUND_SRC_NM"].ToString();
                        fundSrcDTO.data.Add(data);
                    }
                }

                //------------------
                //  關閉資料庫連線
                //------------------

                ConnMSSQL.Close();
                ConnMSSQL.Dispose();

                fundSrcDTO.error_code = ErrorInfo.OK_CODE;
                fundSrcDTO.error_msg = ErrorInfo.OK_MSG;
            }
            catch (Exception ex)
            {
                fundSrcDTO.error_code = ErrorInfo.CATCH_CODE;
                fundSrcDTO.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }



            return fundSrcDTO;

        }

    }
}