﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/UspAoCaseQry


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class UspAoCaseQryDTI
    {
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String APP_CASE_NO { get; set; }
        public String APPL_NO { get; set; }
    }

    public class UspAoCaseQryDTO
    {
        public UspAoCaseQryDTO()
        {
            data = new Data();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public Data data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String APP_CASE_NO { get; set; }
            public String ENTER_DATE { get; set; }
            public String APPL_NO { get; set; }
            public String CH_NAME { get; set; }
            public String CASE_STATUS { get; set; }
            public String PROD_TYPE { get; set; }
            public String PROJ_NAME { get; set; }
            public String CAR_INFO { get; set; }
            public String PAY_BAL { get; set; }
            public String TERM_AMT { get; set; }
            public String TERM_INT { get; set; }
            public String F_AUDIT_RESULT { get; set; }
            public String R_AUDIT_RESULT { get; set; }
            public String EXPLAIN { get; set; }
            public String CHECK_MEMO { get; set; }

        }
    }


    public class UspAoCaseQryDTOo
    {
        public UspAoCaseQryDTOo()
        {
            data = new Data();
        }

        public String error_code { get; set; }
        public String error_msg { get; set; }
        public Data data;

        public class Data
        {
            public String loginResult { get; set; }
            public String errMsg { get; set; }
            public String appCaseNo { get; set; }
            public String enterDate { get; set; }
            public String applNo { get; set; }
            public String chName { get; set; }
            public String caseStatus { get; set; }
            public String prodType { get; set; }
            public String projName { get; set; }
            public String carInfo { get; set; }
            public String payBal { get; set; }
            public String termAmt { get; set; }
            public String tremInt { get; set; }
            public String fAuditResult { get; set; }
            public String rAuditResult { get; set; }
            public String explain { get; set; }
            public String checkMemo { get; set; }
        }
    }

    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class UspAoCaseQryController : ApiController
    {

        // POST api/<controller>
        public UspAoCaseQryDTOo Post(UspAoCaseQryDTI uspAoCaseQryDTI)
        {
            UspAoCaseQryDTOo uspAoCaseQryDTOo = new UspAoCaseQryDTOo();

            try
            {
                string result = new HttpPostClient().exec(WebConfigurationManager.AppSettings["server2_ip"].ToString() + "api/USP_AO_CASE_QRY", uspAoCaseQryDTI);

                var jsonFrom = new JavaScriptSerializer();
                UspAoCaseQryDTO uspAoCaseQryDTO = jsonFrom.Deserialize<UspAoCaseQryDTO>(result);
                if (uspAoCaseQryDTO.ERROR_CODE == ErrorInfo.OK_CODE)
                {
                    uspAoCaseQryDTOo.data.loginResult = uspAoCaseQryDTO.data.LOGIN_RESULT;
                    uspAoCaseQryDTOo.data.errMsg = uspAoCaseQryDTO.data.ERR_MSG;
                    uspAoCaseQryDTOo.data.appCaseNo = uspAoCaseQryDTO.data.APP_CASE_NO;
                    uspAoCaseQryDTOo.data.enterDate = uspAoCaseQryDTO.data.ENTER_DATE;
                    uspAoCaseQryDTOo.data.applNo = uspAoCaseQryDTO.data.APPL_NO;
                    uspAoCaseQryDTOo.data.chName = uspAoCaseQryDTO.data.CH_NAME;
                    uspAoCaseQryDTOo.data.caseStatus = uspAoCaseQryDTO.data.CASE_STATUS;
                    uspAoCaseQryDTOo.data.prodType = uspAoCaseQryDTO.data.PROD_TYPE;
                    uspAoCaseQryDTOo.data.projName = uspAoCaseQryDTO.data.PROJ_NAME;
                    uspAoCaseQryDTOo.data.carInfo = uspAoCaseQryDTO.data.CAR_INFO;
                    uspAoCaseQryDTOo.data.payBal = uspAoCaseQryDTO.data.PAY_BAL;
                    uspAoCaseQryDTOo.data.termAmt = uspAoCaseQryDTO.data.TERM_AMT;
                    uspAoCaseQryDTOo.data.tremInt = uspAoCaseQryDTO.data.TERM_INT;
                    uspAoCaseQryDTOo.data.fAuditResult = uspAoCaseQryDTO.data.F_AUDIT_RESULT;
                    uspAoCaseQryDTOo.data.rAuditResult = uspAoCaseQryDTO.data.R_AUDIT_RESULT;
                    uspAoCaseQryDTOo.data.explain = uspAoCaseQryDTO.data.EXPLAIN;
                    uspAoCaseQryDTOo.data.checkMemo = uspAoCaseQryDTO.data.CHECK_MEMO;
                }
                //---------------------------
                //      Message
                //---------------------------
                uspAoCaseQryDTOo.error_code = uspAoCaseQryDTO.ERROR_CODE;
                uspAoCaseQryDTOo.error_msg = uspAoCaseQryDTO.ERROR_MSG;
            }
            catch (Exception ex)
            {
                uspAoCaseQryDTOo.error_code = ErrorInfo.CATCH_CODE;
                uspAoCaseQryDTOo.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }

            return uspAoCaseQryDTOo;
        }

    }
}