﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/UspAoAppMsgFin


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class UspAoAppMsgFinDTI
    {
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String APP_CASE_NO { get; set; }
    }

    public class UspAoAppMsgFinDTO
    {
        public UspAoAppMsgFinDTO()
        {
            data = new List<Data>();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public List<Data> data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String APP_CASE_NO { get; set; }
            public String MSG_NO { get; set; }
            public String MSG_CONTENT { get; set; }
            public String MSG_DATE { get; set; }
        }
    }


    public class UspAoAppMsgFinDTOo
    {
        public UspAoAppMsgFinDTOo()
        {
            data = new List<Data>();
        }

        public String error_code { get; set; }
        public String error_msg { get; set; }
        public List<Data> data;

        public class Data
        {
            public String loginResult { get; set; }
            public String errMsg { get; set; }
            public String appCaseNo { get; set; }
            public String msgNo { get; set; }
            public String msgContent { get; set; }
            public String msgDate { get; set; }
        }
    }

    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class UspAoAppMsgFinController : ApiController
    {
        // POST api/<controller>
        public UspAoAppMsgFinDTOo Post(UspAoAppMsgFinDTI UspAoAppMsgFinDTI)
        {
            UspAoAppMsgFinDTOo UspAoAppMsgFinDTOo = new UspAoAppMsgFinDTOo();

            try
            {
                string result = new HttpPostClient().exec(WebConfigurationManager.AppSettings["server2_ip"].ToString() + "api/USP_AO_APP_MSG_FIN", UspAoAppMsgFinDTI);

                var jsonFrom = new JavaScriptSerializer();
                UspAoAppMsgFinDTO UspAoAppMsgFinDTO = jsonFrom.Deserialize<UspAoAppMsgFinDTO>(result);
                if (UspAoAppMsgFinDTO.ERROR_CODE == ErrorInfo.OK_CODE)
                {
                    foreach (UspAoAppMsgFinDTO.Data _data in UspAoAppMsgFinDTO.data)
                    {
                        UspAoAppMsgFinDTOo.Data data = new UspAoAppMsgFinDTOo.Data();

                        data.loginResult = _data.LOGIN_RESULT;
                        data.errMsg = _data.ERR_MSG;
                        data.appCaseNo = _data.APP_CASE_NO;
                        data.msgNo = _data.MSG_NO;
                        data.msgContent = _data.MSG_CONTENT;
                        data.msgDate = _data.MSG_DATE;
                    
                        UspAoAppMsgFinDTOo.data.Add(data);
                    }
                }
                UspAoAppMsgFinDTOo.data = UspAoAppMsgFinDTOo.data.OrderByDescending(x => x.msgNo).ToList();
                //---------------------------
                //      Message
                //---------------------------
                UspAoAppMsgFinDTOo.error_code = UspAoAppMsgFinDTO.ERROR_CODE;
                UspAoAppMsgFinDTOo.error_msg = UspAoAppMsgFinDTO.ERROR_MSG;
            }
            catch (Exception ex)
            {
                UspAoAppMsgFinDTOo.error_code = ErrorInfo.CATCH_CODE;
                UspAoAppMsgFinDTOo.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }

            return UspAoAppMsgFinDTOo;
        }

    }
}