﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AZURE_Server1
{
    class BlobMain
    {
        //=========================================================
        //          Upload
        //=========================================================
        public class ResultStuts
        {
            public String CODE { get; set; }
            public String MSG { get; set; }
        }

        //=========================================================
        //          Upload
        //=========================================================
        public void Upload()
        {
            //Console.WriteLine("Azure Blob storage - .NET Quickstart sample");
            //Console.WriteLine();
            //ProcessAsync().GetAwaiter().GetResult();

            //------------------------------------------------------
            BlobFunc blobFunc = new BlobFunc(BlobConstants.STORAGE_CONNECTION_STRING, BlobConstants.CONTAINER_NAME, BlobConstants.DISK_FOLDER_NAME);
            List<string> blobs = blobFunc.GetListBlobs();


            //------------------------------------------------------
            FolderFunc folderFunc = new FolderFunc(BlobConstants.DISK_FOLDER_NAME);
            List<string> files = folderFunc.GetFolderEntries();

            //------------------------------------------------------
            blobFunc.UploadBlob("myBlob.jpg");
        }

        public string UploadAndBlobUrl(string containerName, string zipFolder, string fileName)
        {
            BlobFunc blobFunc = new BlobFunc(
                BlobConstants.STORAGE_CONNECTION_STRING,
                containerName,
                zipFolder);
            return blobFunc.UploadBlobAndReturnUrl(fileName);
        }
        //=========================================================
        //          Download
        //=========================================================
        public void Download()
        {
            //Console.WriteLine("Azure Blob storage - .NET Quickstart sample");
            //Console.WriteLine();
            //ProcessAsync().GetAwaiter().GetResult();


            //------------------------------------------------------
            BlobFunc blobFunc = new BlobFunc(BlobConstants.STORAGE_CONNECTION_STRING, BlobConstants.CONTAINER_NAME, BlobConstants.DISK_FOLDER_NAME);
            List<string> blobs = blobFunc.GetListBlobs();


            //------------------------------------------------------
            FolderFunc folderFunc = new FolderFunc(BlobConstants.DISK_FOLDER_NAME);
            List<string> files = folderFunc.GetFolderEntries();

            //------------------------------------------------------
            foreach (string blob in blobs)
            {
                int yes = 0;
                foreach (string file in files)
                {
                    if (file == blob)
                    {
                        yes = 1;
                    }
                }
                if (yes == 0)
                {
                    blobFunc.DownloadBlobs(blob);
                    Console.WriteLine("Download : " + blob + "\n");
                }
            }

             
            //------------------------------------------------------
            //Console.WriteLine("Press any key to exit the sample application.");
            //Console.ReadLine();
        }

        public void Download(string fileName, string container_name, string path)
        {
            //Console.WriteLine("Azure Blob storage - .NET Quickstart sample");
            //Console.WriteLine();
            //ProcessAsync().GetAwaiter().GetResult();


            //------------------------------------------------------
            BlobFunc blobFunc = new BlobFunc(Constants.STORAGE_CONNECTION_STRING, container_name, path);
            List<string> blobs = blobFunc.GetListBlobs();


            //------------------------------------------------------
            FolderFunc folderFunc = new FolderFunc(path);
            List<string> files = folderFunc.GetFolderEntries();

            //------------------------------------------------------
            foreach (string blob in blobs)
            {
                if (blob == fileName)
                {
                    blobFunc.DownloadBlobs(blob, path);
                }
            }


            //------------------------------------------------------
            //Console.WriteLine("Press any key to exit the sample application.");
            //Console.ReadLine();
        }

        public void NotExistsCreate(string container_name)
        {
            BlobFunc blobFunc = new BlobFunc(Constants.STORAGE_CONNECTION_STRING, container_name, "");
            blobFunc.CreateBlobContainer();

        }
        //=========================================================
        //          Delete
        //=========================================================
        public ResultStuts Delete(string fileName)
        {
            //Console.WriteLine("Azure Blob storage - .NET Quickstart sample");
            //Console.WriteLine();
            //ProcessAsync().GetAwaiter().GetResult();

            ResultStuts resultStuts = new ResultStuts();
            resultStuts.CODE = "";
            resultStuts.MSG = "";

            //------------------------------------------------------
            BlobFunc blobFunc = new BlobFunc(BlobConstants.STORAGE_CONNECTION_STRING, BlobConstants.CONTAINER_NAME, BlobConstants.DISK_FOLDER_NAME);
            List<string> blobs = blobFunc.GetListBlobs();


            //------------------------------------------------------
            FolderFunc folderFunc = new FolderFunc(BlobConstants.DISK_FOLDER_NAME);
            List<string> files = folderFunc.GetFolderEntries();

            //------------------------------------------------------
            if (blobFunc.FindBlob(fileName) == 1)
            {
                blobFunc.DeleteBlob(fileName);
            }
            else
            {
                //Console.WriteLine("Can not find " + fileName + " file in BLOB_Server");
                resultStuts.CODE = "402";
                resultStuts.MSG = "Can not find " + fileName + " file in BLOB_Server\n";
            }
            
            if (folderFunc.FindFile(fileName) == 1)
            {
                folderFunc.DeleteFile(fileName);
            }
            else
            {
                //Console.WriteLine("Can not find " + fileName + " file in disk");
                resultStuts.CODE = "402";
                resultStuts.MSG += "Can not find " + fileName + " file in disk";
            }

            if(resultStuts.CODE == "")
            {
                resultStuts.CODE = "200";
                resultStuts.MSG += "成功";
            }

            return resultStuts;
        }
    }
}
