﻿using AZURE_Server1.DB;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AZURE_Server1.WebApiControllers
{
    public class FixCaseListDTI
    {
        public string SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
    }

    public class FixCaseListDTO
    {
        public String error_code { get; set; }
        public String error_msg { get; set; }

        public List<Data> data { get; set; }

        public class Data {

            public string LOGIN_RESULT { get; set; }
            public string ERR_MSG { get; set; }
            public string ERR_NO { get; set; }
            public string ENTER_DATE { get; set; }
            public string APP_CASE_NO { get; set; }
            public string CH_NAME { get; set; }
        }

    }

    public class FixCaseListApiDTO
    {
        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }

        public List<Data> data { get; set; }

        public class Data
        {
            public string LOGIN_RESULT { get; set; }
            public string ERR_MSG { get; set; }
            public string ERR_NO { get; set; }
            public string ENTER_DATE { get; set; }
            public string APP_CASE_NO { get; set; }
            public string CH_NAME { get; set; }
            public string P_ID { get; set; }
            public string COM_FLG { get; set; }
            public string GUARD1_ID { get; set; }
            public string GUARD1_NAME { get; set; }
            public string GUARD2_ID { get; set; }
            public string GUARD2_NAME { get; set; }
            public string GUARD3_ID { get; set; }
            public string GUARD3_NAME { get; set; }
            public string GUARD4_ID { get; set; }
            public string GUARD4_NAME { get; set; }
            public string GUARD5_ID { get; set; }
            public string GUARD5_NAME { get; set; }
        }
    }

    public class AppInfoModel
    {
        public AppDataModel APP_DATA { get; set; }

        public PicDataModel PIC_DATA { get; set; }
    }

    public class AppDataModel
    {
        public string CUS_NAME {get; set;}
        public string REMARK { get; set;}
        public string BRNH_NO {get; set;}  
        public string GURD_NAME_04 {get; set;}  
        public string APPL_NO {get; set;}  
        public string GURD_ID_01 {get; set;}  
        public string BANK_NA {get; set;}  
        public string PROD_TYPE_NM { get; set;}  
        public string PROJ_ID_NM { get; set;}  
        public string NOQRY_FLG { get; set;}  
        public string APP_BACK_NOTE { get; set;}  
        public string GURD_NAME_03 { get; set;}
        public string DLR_NO_NM { get; set;}  
        public string SPEC_TIME { get; set;}  
        public string SALES_NO_NM { get; set;}  
        public string GURD_ID_04 { get; set;}  
        public string BANK_NA_NM { get; set;}   
        public string APP_FORM_1 { get; set;}  
        public string SALES_NO { get; set;}  
        public string GURD_ID_02 { get; set;}  
        public string CASE_TYPE { get; set;}  
        public string BRNH_NO_NM { get; set;}  
        public string GURD_NAME_02 { get; set;}  
        public string ORI_CASE_NO { get; set;}  
        public string GURD_NAME_05 { get; set;}  
        public string PROD_TYPE { get; set;}  
        public string CUST_NO { get; set;}  
        public string GURD_ID_05 { get; set;}  
        public string FUND_SRC_NM { get; set;}  
        public string PROJ_ID { get; set;}  
        public string GURD_NAME_01 { get; set;}  
        public string DLR_NO { get; set;}  
        public string FUND_SRC { get; set;}  
        public string APP_CASE_NO { get; set;} 
        public string GURD_ID_03 { get; set;} 
        public AppDataModel()
        {
              CUS_NAME = String.Empty;
              REMARK = String.Empty;
              BRNH_NO = String.Empty;
              GURD_NAME_04 = String.Empty;
              APPL_NO = String.Empty;
              GURD_ID_01 = String.Empty;
              BANK_NA = String.Empty;
              PROD_TYPE_NM = String.Empty;
              PROJ_ID_NM = String.Empty;
              NOQRY_FLG = String.Empty;
              APP_BACK_NOTE = String.Empty;
              GURD_NAME_03 = String.Empty;
              DLR_NO_NM = String.Empty;
              SPEC_TIME = String.Empty;
              SALES_NO_NM = String.Empty;
              GURD_ID_04 = String.Empty;
              BANK_NA_NM = String.Empty;
              APP_FORM_1 = String.Empty;
              SALES_NO = String.Empty;
              GURD_ID_02 = String.Empty;
              CASE_TYPE = String.Empty;
              BRNH_NO_NM = String.Empty;
              GURD_NAME_02 = String.Empty;
              ORI_CASE_NO = String.Empty;
              GURD_NAME_05 = String.Empty;
              PROD_TYPE = String.Empty;
              CUST_NO = String.Empty;
              GURD_ID_05 = String.Empty;
              FUND_SRC_NM = String.Empty;
              PROJ_ID = String.Empty;
              GURD_NAME_01 = String.Empty;
              DLR_NO = String.Empty;
              FUND_SRC = String.Empty;
              APP_CASE_NO = String.Empty;
              GURD_ID_03 = String.Empty;
        }
    }

    public class PicDataModel
    {
        public string PRICE_DOC { get;set;}
        public string APP_BACK_DOC { get;set;}
        public string PAY_PIC_01 { get;set;}
        public string LIC_DOC { get;set;}
        public string CAR_FORM { get;set;}
        public string CAR_PIC { get;set;}
        public string PATCH_DATA { get;set;}
        public string OTHER_DATA { get;set;}
        public string APP_DOC { get;set;}
        public string BANK_DOC { get;set;}
        public string PAY_PIC_02 { get;set;}   

        public OwnerDataModel OWNER_DATA { get; set; }
        public List<GurdDataModel> GURD_DATA {get;set;}

        public PicDataModel()
        {
            OWNER_DATA = new OwnerDataModel();
            GURD_DATA = new List<GurdDataModel>()
            {
                new GurdDataModel(){ GURD_NO="1" },
                new GurdDataModel(){ GURD_NO="2" },
                new GurdDataModel(){ GURD_NO="3" },
                new GurdDataModel(){ GURD_NO="4" },
                new GurdDataModel(){ GURD_NO="5" },
            };

            PRICE_DOC = String.Empty;
             APP_BACK_DOC = String.Empty;
             PAY_PIC_01 = String.Empty;
             LIC_DOC = String.Empty;
             CAR_FORM = String.Empty;
             CAR_PIC = String.Empty;
             PATCH_DATA = String.Empty;
             OTHER_DATA = String.Empty;
             APP_DOC = String.Empty;
             BANK_DOC = String.Empty;
             PAY_PIC_02 = String.Empty;
        }
    }

    public class OwnerDataModel
    {
        public string COMP_DATA  {get;set;}
        public string OWNER_ID_4  {get;set;}
        public string OWNER_ID_2  {get;set;}
        public string OWNER_ID_5  {get;set;}
        public string OWNER_ID_3  {get;set;}
        public string OWNER_ID_1  {get;set;}
        public OwnerDataModel()
        {
            COMP_DATA = string.Empty;
            OWNER_ID_4 = string.Empty;
            OWNER_ID_2 = string.Empty;
            OWNER_ID_5 = string.Empty;
            OWNER_ID_3 = string.Empty;
            OWNER_ID_1 = string.Empty;
        }
    }
    
    public class GurdDataModel
    {
        public string GURD_ID_3 { get;set;}
        public string GURD_ID_4 { get;set;}
        public string GURD_ID_1 { get;set;}
        public string GURD_ID_5 { get;set;}
        public string GURD_ID_2 { get;set;}
        public string GURD_NO { get;set;}
        public GurdDataModel()
        {
            GURD_ID_3 = string.Empty;
            GURD_ID_4 = string.Empty;
            GURD_ID_1 = string.Empty;
            GURD_ID_5 = string.Empty;
            GURD_ID_2 = string.Empty;
            GURD_NO = string.Empty;
        }
    }

    public class FixCaseListController : ApiController
    {
        /// <summary>
        /// 修復近50筆案件清單
        /// </summary>
        /// <param name="FixCaseListDTI"></param>
        /// <returns></returns>
        public FixCaseListDTO Post(FixCaseListDTI input)
        {
            FixCaseListDTO output = new FixCaseListDTO();
            output.data = new List<FixCaseListDTO.Data>();

            Logger logger = NLog.LogManager.GetCurrentClassLogger();

            try
            {
                //修復近50筆案件清單 call sp
                string result = new HttpPostClient().exec(WebConfigurationManager.AppSettings["server2_ip"].ToString() + "api/USP_AO_RECOVER_LIST", input);
                logger.Info("result="+ result);
                var jsonFrom = new JavaScriptSerializer();
                FixCaseListApiDTO uspAoAppMsgDTO = JsonConvert.DeserializeObject<FixCaseListApiDTO>(result);

                logger.Info("uspAoAppMsgDTO.data.Count=" + uspAoAppMsgDTO.data.Count);

                if (uspAoAppMsgDTO.ERROR_CODE == ErrorInfo.OK_CODE)
                {
                    using (var db = new EasyPayEntities())
                    {
                        int addCount = 0;
                        int updateCount = 0;
                        foreach (var spItem in uspAoAppMsgDTO.data)
                        {
                            logger.Info("item=" + JsonConvert.SerializeObject(spItem));
                            FixCaseListDTO.Data d = new FixCaseListDTO.Data();
                            d.ENTER_DATE = spItem.ENTER_DATE;
                            d.APP_CASE_NO = spItem.APP_CASE_NO;
                            d.CH_NAME = spItem.CH_NAME;
                            d.LOGIN_RESULT = spItem.LOGIN_RESULT;
                            d.ERR_MSG = spItem.ERR_MSG;
                            d.ERR_NO = spItem.ERR_NO;
                            output.data.Add(d);

                            //代表有撈到資料可以做資料庫add/update
                            logger.Info("item.LOGIN_RESULT.ToLower()=" + spItem.LOGIN_RESULT.ToLower());
                            if (spItem.LOGIN_RESULT.ToLower() == "y")
                            {
                                int oldId = 0;
                                var caseList = db.Case.Where(p =>p.mobile_no == input.MOBILE_NO);
                                var caseTestList = db.CaseTest.Where(p => p.mobile_no == input.MOBILE_NO);
                                if (Convert.ToBoolean(WebConfigurationManager.AppSettings["is_production"]))
                                {
                                    var oldData = caseList.FirstOrDefault(p => p.app_case_noe == spItem.APP_CASE_NO);
                                    if (oldData != null) oldId = oldData.id;
                                }
                                else
                                {
                                    var oldData = caseTestList.FirstOrDefault(p => p.app_case_noe == spItem.APP_CASE_NO);
                                    if (oldData != null) oldId = oldData.id;
                                }
                                logger.Info("oldId=" + oldId);
                                if (oldId == 0)
                                {
                                    //組app_info
                                    AppInfoModel appInfo = new AppInfoModel();
                                    appInfo.APP_DATA = new AppDataModel();
                                    appInfo.PIC_DATA = new PicDataModel();
                                    appInfo.APP_DATA.CUS_NAME = spItem.CH_NAME;
                                    appInfo.APP_DATA.CUST_NO = spItem.P_ID;  appInfo.APP_DATA.GURD_ID_01 = spItem.GUARD1_ID;
                                    appInfo.APP_DATA.GURD_NAME_01 = spItem.GUARD1_NAME;
                                    appInfo.APP_DATA.APP_CASE_NO = spItem.APP_CASE_NO;
                                    appInfo.APP_DATA.CASE_TYPE = "01";
                                    appInfo.APP_DATA.GURD_ID_01 = spItem.GUARD1_ID;
                                    appInfo.APP_DATA.GURD_NAME_01 = spItem.GUARD1_NAME;
                                    appInfo.APP_DATA.GURD_ID_02 = spItem.GUARD2_ID;
                                    appInfo.APP_DATA.GURD_NAME_02 = spItem.GUARD2_NAME;
                                    appInfo.APP_DATA.GURD_ID_03 = spItem.GUARD3_ID;
                                    appInfo.APP_DATA.GURD_NAME_03 = spItem.GUARD3_NAME;
                                    appInfo.APP_DATA.GURD_ID_04 = spItem.GUARD4_ID;
                                    appInfo.APP_DATA.GURD_NAME_04 = spItem.GUARD4_NAME;
                                    appInfo.APP_DATA.GURD_ID_05 = spItem.GUARD5_ID;
                                    appInfo.APP_DATA.GURD_NAME_05 = spItem.GUARD5_NAME;

                                    var appInfoJson = JsonConvert.SerializeObject(appInfo);

                                    if (Convert.ToBoolean(WebConfigurationManager.AppSettings["is_production"]))
                                    {
                                        db.Case.Add(new Case()
                                        {
                                            app_case_noe = spItem.APP_CASE_NO,
                                            app_info = appInfoJson,
                                            case_type = 1,
                                            create_time = DateTime.Parse(spItem.ENTER_DATE),
                                            cust_name = spItem.CH_NAME,
                                            ori_case_no = spItem.APP_CASE_NO,
                                            img_root_url = "",
                                            mobile_no = input.MOBILE_NO
                                        });
                                    }
                                    else
                                    {
                                        db.CaseTest.Add(new CaseTest()
                                        {
                                            app_case_noe = spItem.APP_CASE_NO,
                                            app_info = appInfoJson,
                                            case_type = 1,
                                            create_time = DateTime.Parse(spItem.ENTER_DATE),
                                            cust_name = spItem.CH_NAME,
                                            ori_case_no = spItem.APP_CASE_NO,
                                            img_root_url = "",
                                            mobile_no = input.MOBILE_NO
                                        });
                                    }
                                        
                                    addCount++;
                                    logger.Info("addCount=" + addCount);
                                }
                                else
                                {
                                    //修改 skip
                                    updateCount++;
                                    logger.Info("updateCount=" + updateCount);
                                }
                            }
                        }

                        int saveCount = db.SaveChanges();
                        if (addCount == saveCount)
                        {
                            output.error_code = "200";
                            output.error_msg = "成功,新增" + addCount + "筆;略過" + updateCount + "筆";
                        }
                        else
                        {
                            output.error_code = "400";
                            output.error_msg = "上傳失敗";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                output.error_code = "900";
                output.error_msg += ex.ToString();
            }
            return output;
        }

    }
}