﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AZURE_Server1
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/Login


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class LoginDTI
    {
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String MOBILE_TYPE { get; set; }
        public String OS_NAME { get; set; }
        public String OS_VERNO { get; set; }
    }

    public class TagNoDTI
    {
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String TAG_NO { get; set; }
    }




    public class LoginDTO
    {
        public LoginDTO()
        {
            data = new Data();
        }
        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public Data data;
        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_NO { get; set; }
            public String ERR_MSG { get; set; }
            public String FTP_IP { get; set; }
            public String FTP_PORT { get; set; }
            public String FTP_ACCT { get; set; }
            public String FTP_PWD { get; set; }
            public String PARAM_TIMER { get; set; }
            public String TAG_NO { get; set; }
            public String POST_URL { get; set; }
            public String BD_Timer { get; set; }
            public String DOWNLOAD_FLG { get; set; }
            public String MEM_LIMIT { get; set; }
        }
    }

    public class TagNoDTO
    {
        public TagNoDTO()
        {
            data = new Data();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public Data data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String ERR_NO { get; set; }
            public String TAG_NO { get; set; }
        }
    }
    public class LoginDTOo
    {
        public LoginDTOo()
        {
            data = new Data();
        }
        public String error_code { get; set; }
        public String error_msg { get; set; }
        public Data data;

        public class Data
        {
            public String loginResult { get; set; }
            public String errNo { get; set; }
            public String errMsg { get; set; }
            public String ftpIp { get; set; }
            public String ftpPort { get; set; }
            public String ftpAcct { get; set; }
            public String ftpPwd { get; set; }
            public String paramTimer { get; set; }
            public String tagNo { get; set; }
            public String postURL { get; set; }
            public String bdTimer { get; set; }
            public String downloadFLG { get; set; }
            public String memLimit { get; set; }
            public String tokenData { get; set; }
            public String PPID { get; set; }
        }


    }

    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class LoginController : ApiController
    {
        public class OutputModel
        {
            public OutputModel()
            {
                Data = new OutputDataModel();
            }

            public String ERROR_CODE { get; set; }
            public String ERROR_MSG { get; set; }

            public OutputDataModel Data;
        }

        public class OutputDataModel
        {
            public String EMPL_ID { get; set; }
            public String PASWD { get; set; }
            public String USER_LOGIN_ID { get; set; }
        }


        // GET api/<controller>/5
        //public object Get()
        //{
        //    return Redirect("http://localhost:59025/api/TestApi");
        //}

        // POST api/<controller>
        public LoginDTOo Post(LoginDTI loginDTI)
        {
            LoginDTOo loginDTOo = new LoginDTOo();

            try
            {
                string result = new HttpPostClient().exec(WebConfigurationManager.AppSettings["server2_ip"].ToString() + "api/USP_AO_APP_LOGIN", loginDTI);

                var jsonFrom = new JavaScriptSerializer();
                LoginDTO loginDTO = jsonFrom.Deserialize<LoginDTO>(result);
                if(loginDTO.ERROR_CODE=="200")
                {
                    loginDTOo.data.loginResult = loginDTO.data.LOGIN_RESULT;
                    loginDTOo.data.errNo = loginDTO.data.ERR_NO;
                    loginDTOo.data.errMsg = loginDTO.data.ERR_MSG;
                    loginDTOo.data.ftpIp = loginDTO.data.FTP_IP;
                    loginDTOo.data.ftpPort = loginDTO.data.FTP_PORT;
                    loginDTOo.data.ftpAcct = loginDTO.data.FTP_ACCT;
                    loginDTOo.data.ftpPwd = loginDTO.data.FTP_PWD;
                    loginDTOo.data.paramTimer = loginDTO.data.PARAM_TIMER;
                    loginDTOo.data.tagNo = loginDTO.data.TAG_NO;

                    if (string.IsNullOrEmpty(loginDTO.data.TAG_NO))
                    {
                        //若為null 則呼叫sp寫入
                        try
                        {
                           string resultTagNo = new HttpPostClient().exec(
                           WebConfigurationManager.AppSettings["server2_ip"].ToString() + "api/USP_AO_REG_TAG"
                           , new TagNoDTI()

                           {
                               MOBILE_ID = loginDTI.MOBILE_ID,
                               MOBILE_NO = loginDTI.MOBILE_NO,
                               SALES_NO = loginDTI.SALES_NO,
                               TAG_NO = DateTime.Now.ToString("yyyyMMddHHmmss") + "-" + Guid.NewGuid().ToString()
                           });

                            TagNoDTO tagNoDTO = jsonFrom.Deserialize<TagNoDTO>(resultTagNo);
                            if (tagNoDTO.ERROR_CODE == "200")
                            {
                                ///成功則覆蓋實際tageNo
                                loginDTOo.data.tagNo = tagNoDTO.data.TAG_NO;
                            }
                        }
                        catch(Exception e)
                        {
                            loginDTO.ERROR_MSG += "get tagNo fail , exception msg:"+e.Message;
                        }
                    }

                    loginDTOo.data.postURL = loginDTO.data.POST_URL;
                    loginDTOo.data.bdTimer = loginDTO.data.BD_Timer;
                    loginDTOo.data.downloadFLG = loginDTO.data.DOWNLOAD_FLG;
                    loginDTOo.data.memLimit = loginDTO.data.MEM_LIMIT;

                    string result2 = new HttpPostClient().exec(WebConfigurationManager.AppSettings["server2_ip"].ToString() + "api/USP_AO_QRY_OLOGIN", loginDTI);
                    OutputModel loginDTO2 = jsonFrom.Deserialize<OutputModel>(result2);
                    string loginResult = postLoginApi(loginDTO2);
                    loginResultModel loginDTO3 = jsonFrom.Deserialize<loginResultModel>(loginResult);

                    loginDTOo.data.PPID = loginDTO2.Data.EMPL_ID;
                    loginDTOo.data.tokenData = loginDTO3.Token;
                   
                }
                //---------------------------
                //      Message
                //---------------------------
                loginDTOo.error_code = loginDTO.ERROR_CODE;
                loginDTOo.error_msg = loginDTO.ERROR_MSG;
            }
            catch (Exception ex)
            {
                loginDTOo.error_code = ErrorInfo.CATCH_CODE;
                loginDTOo.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }

            return loginDTOo;
        }

        public class loginResultModel
        {
            public string Token { get; set; }
        }


        private string postLoginApi(OutputModel model)
        {
            string url = WebConfigurationManager.AppSettings["login_ip"].ToString();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            //必須透過ParseQueryString()來建立NameValueCollection物件，之後.ToString()才能轉換成queryString
            NameValueCollection postParams = System.Web.HttpUtility.ParseQueryString(string.Empty);
            postParams.Add("PPID", model.Data.EMPL_ID);
            postParams.Add("PPPW", model.Data.PASWD);
            postParams.Add("USERNAME", model.Data.USER_LOGIN_ID);

            //Console.WriteLine(postParams.ToString());// 將取得"version=1.0&action=preserveCodeCheck&pCode=pCode&TxID=guid&appId=appId", key和value會自動UrlEncode
            //要發送的字串轉為byte[] 
            byte[] byteArray = Encoding.UTF8.GetBytes(postParams.ToString());
            using (Stream reqStream = request.GetRequestStream())
            {
                reqStream.Write(byteArray, 0, byteArray.Length);
            }//end using

            //API回傳的字串
            string responseStr = "";
            //發出Request
            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    responseStr = sr.ReadToEnd();
                }//end using  
            }

            return responseStr;
        }

    }
}