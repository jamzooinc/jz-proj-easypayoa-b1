﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/SalesNo



    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class SalesNoDTI : LoginDTI
    {
        /*
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String MOBILE_TYPE { get; set; }
        public String OS_NAME { get; set; }
        public String OS_VERNO { get; set; }
        */
        public String SALES_SELECT { get; set; }
        public String SALES_NO_NM { get; set; }
    }

    public class SalesNoDTO
    {
        public SalesNoDTO()
        {
            data = new List<Data>();
        }
        public String error_code { get; set; }
        public String error_msg { get; set; }
        public List<Data> data;
        public class Data
        {
            public String salesNo { get; set; }
            public String salesNonm { get; set; }
        }
    }


    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class SalesNoController : ApiController
    {


        // POST api/<controller>
        public SalesNoDTO Post(SalesNoDTI salesNoDTI)
        {
            LoginDTOo loginDTOo = new LoginController().Post(salesNoDTI);

            SalesNoDTO salesNoDTO = new SalesNoDTO();
            if (loginDTOo.data.loginResult == "Y")
            {
                salesNoDTO = get(salesNoDTI);
                salesNoDTO.error_code = ErrorInfo.OK_CODE;
                salesNoDTO.error_msg = ErrorInfo.OK_MSG;
            }
            else
            {
                salesNoDTO.error_code = ErrorInfo.IDNG_CODE;
                salesNoDTO.error_msg = ErrorInfo.IDNG_MSG;
            }
            return salesNoDTO;
        }

        private SalesNoDTO get(SalesNoDTI salesNoDTI)
        {
            SalesNoDTO salesNoDTO = new SalesNoDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["AE_DB"].ToString();

                SqlConnection ConnMSSQL = new SqlConnection(connectionString);
                ConnMSSQL.Open();

#if false
                String commStr = @"SELECT TOP(1000) [SALES_NO], [SALES_NO_NM]
                                                FROM[AE].[dbo].[AE_SALES_NO]
                                                WHERE SALES_NO like @salesNo AND SALES_NO_NM like @salesNoNM";

                SqlCommand cmd = new SqlCommand(commStr, ConnMSSQL);
                cmd.Parameters.AddWithValue("@salesNo", "%"+ salesNoDTI.SALES_SELECT +"%");
                cmd.Parameters.AddWithValue("@salesNoNM", "%" + salesNoDTI.SALES_NO_NM + "%");
#endif

                String commStr = @"SELECT TOP(1000) [SALES_NO], [SALES_NO_NM]
                                                FROM[AE].[dbo].[AE_SALES_NO]";

                int cntParams = 0;
                if ( (salesNoDTI.SALES_SELECT != null) && (salesNoDTI.SALES_SELECT != "") )
                {
                    if (cntParams == 0)
                    {
                        commStr += @" WHERE  SALES_NO like @salesNo";
                    }
                    else
                    {
                        commStr += @" AND";
                        commStr += @" SALES_NO like @salesNo";
                    }
                    cntParams++;
                }
                if ((salesNoDTI.SALES_NO_NM != null) && (salesNoDTI.SALES_NO_NM != ""))
                {
                    if (cntParams == 0)
                    {
                        commStr += @" WHERE SALES_NO_NM like @salesNoNM";
                    }
                    else
                    {
                        commStr += @" AND";
                        commStr += @" SALES_NO_NM like @salesNoNM";
                    }
                    cntParams++;
                }
                if (cntParams == 0)
                {
                    commStr += @";";
                }

                SqlCommand cmd = new SqlCommand(commStr, ConnMSSQL);
                if ((salesNoDTI.SALES_SELECT != null) && (salesNoDTI.SALES_SELECT != ""))
                {
                    cmd.Parameters.AddWithValue("@salesNo", "%" + salesNoDTI.SALES_SELECT + "%");
                }
                if ((salesNoDTI.SALES_NO_NM != null) && (salesNoDTI.SALES_NO_NM != ""))
                {
                    cmd.Parameters.AddWithValue("@salesNoNM", "%" + salesNoDTI.SALES_NO_NM + "%");
                }
               

                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    int cnt = 0;
                    while (dr.Read())
                    {
                        SalesNoDTO.Data data = new SalesNoDTO.Data();
                        data.salesNo = dr["SALES_NO"].ToString();
                        data.salesNonm = dr["SALES_NO_NM"].ToString();

                        if(cnt!=50)
                        {
                            cnt++;
                            salesNoDTO.data.Add(data);
                        }
                    }
                }

                //------------------
                //  關閉資料庫連線
                //------------------

                ConnMSSQL.Close();
                ConnMSSQL.Dispose();

                salesNoDTO.error_code = ErrorInfo.OK_CODE;
                salesNoDTO.error_msg = ErrorInfo.OK_MSG;
            }
            catch (Exception ex)
            {
                salesNoDTO.error_code = ErrorInfo.CATCH_CODE;
                salesNoDTO.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }



            return salesNoDTO;

        }
    }
}