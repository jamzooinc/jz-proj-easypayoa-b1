﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/IdentifierUpdate



    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class IdentifierUpdateDTI
    {
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
    }


    public class IdentifierUpdateDTO
    {
        public IdentifierUpdateDTO()
        {
            data = new Data();
        }
        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public Data data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String ERR_NO { get; set; }
        }

    }

    public class IdentifierUpdateDTOo
    {
        public IdentifierUpdateDTOo()
        {
            data = new Data();
        }
        public String error_code { get; set; }
        public String error_msg { get; set; }
        public Data data;

        public class Data
        {
            public String loginResult { get; set; }
            public String errNo { get; set; }
            public String errMsg { get; set; }
        }

    }


    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class IdentifierUpdateController : ApiController
    {


        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public IdentifierUpdateDTOo Post(IdentifierUpdateDTI identifierUpdateDTI)
        {
            IdentifierUpdateDTOo identifierUpdateDTOo = new IdentifierUpdateDTOo();

            try
            {
                string result = new HttpPostClient().exec(WebConfigurationManager.AppSettings["server2_ip"].ToString() + "api/USP_AO_AGENT_UPD", identifierUpdateDTI);

                var jsonFrom = new JavaScriptSerializer();
                IdentifierUpdateDTO identifierUpdateDTO = jsonFrom.Deserialize<IdentifierUpdateDTO>(result);

                if (identifierUpdateDTO.ERROR_CODE == "200")
                {
                    identifierUpdateDTOo.data.loginResult = identifierUpdateDTO.data.LOGIN_RESULT;
                    identifierUpdateDTOo.data.errNo = identifierUpdateDTO.data.ERR_NO;
                    identifierUpdateDTOo.data.errMsg = identifierUpdateDTO.data.ERR_MSG;
                }
                //---------------------------
                //      Message
                //---------------------------
                identifierUpdateDTOo.error_code = identifierUpdateDTO.ERROR_CODE;
                identifierUpdateDTOo.error_msg = identifierUpdateDTO.ERROR_MSG;

            }
            catch (Exception ex)
            {
                identifierUpdateDTOo.error_code = ErrorInfo.CATCH_CODE;
                identifierUpdateDTOo.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }


            return identifierUpdateDTOo;
        }


    }
}