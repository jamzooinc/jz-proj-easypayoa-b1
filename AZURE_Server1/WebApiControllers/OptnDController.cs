﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/OptnD



    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class OptnDDTI : LoginDTI
    {
        /*
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String MOBILE_TYPE { get; set; }
        public String OS_NAME { get; set; }
        public String OS_VERNO { get; set; }
        */
    }

    public class OptnDDTO
    {
        public OptnDDTO()
        {
            data = new List<Data>();
        }
        public String error_code { get; set; }
        public String error_msg { get; set; }
        public List<Data> data;
        public class Data
        {
            public String classID { get; set; }
            public String className { get; set; }
            public String leftOptnID { get; set; }
            public String leftOptnName { get; set; }
            public String rightOptnID { get; set; }
            public String rightOptnName { get; set; }
        }
    }


    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class OptnDController : ApiController
    {


        // POST api/<controller>
        public OptnDDTO Post(OptnDDTI optnDDTI)
        {
            LoginDTOo loginDTOo = new LoginController().Post(optnDDTI);

            OptnDDTO optnDDTO = new OptnDDTO();
            if (loginDTOo.data.loginResult == "Y")
            {
                optnDDTO = get();
                optnDDTO.error_code = ErrorInfo.OK_CODE;
                optnDDTO.error_msg = ErrorInfo.OK_MSG;
            }
            else
            {
                optnDDTO.error_code = ErrorInfo.IDNG_CODE;
                optnDDTO.error_msg = ErrorInfo.IDNG_MSG;
            }
            return optnDDTO;
        }

        private OptnDDTO get()
        {
            OptnDDTO optnDDTO = new OptnDDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["AE_DB"].ToString();

                SqlConnection ConnMSSQL = new SqlConnection(connectionString);
                ConnMSSQL.Open();

                String testStoreProcedureStr = @"SELECT * FROM AE_OPTN_D;";
                SqlCommand cmd = new SqlCommand(testStoreProcedureStr, ConnMSSQL);
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        OptnDDTO.Data data = new OptnDDTO.Data();

                        data.classID = dr["CLASSID"].ToString();
                        data.className = dr["CLASSNAME"].ToString();
                        data.leftOptnID = dr["L_OPTN_ID"].ToString();
                        data.leftOptnName = dr["L_OPTN_NAME"].ToString();
                        data.rightOptnID = dr["R_OPTN_ID"].ToString();
                        data.rightOptnName = dr["R_OPTN_NAME"].ToString();

                        optnDDTO.data.Add(data);
                    }
                }

                //------------------
                //  關閉資料庫連線
                //------------------

                ConnMSSQL.Close();
                ConnMSSQL.Dispose();

                optnDDTO.error_code = ErrorInfo.OK_CODE;
                optnDDTO.error_msg = ErrorInfo.OK_MSG;
            }
            catch (Exception ex)
            {
                optnDDTO.error_code = ErrorInfo.CATCH_CODE;
                optnDDTO.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }



            return optnDDTO;

        }


    }
}