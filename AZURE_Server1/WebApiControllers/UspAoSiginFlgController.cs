﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/UspAoSiginFlg


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class UspAoSiginFlgDTI
    {
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String appCaseNo { get; set; }
        public String applNo { get; set; }
    }

    public class UspAoSiginFlgDTO
    {
        public UspAoSiginFlgDTO()
        {
            data = new List<Data>();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public List<Data> data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
         
        }
    }


    public class UspAoSiginFlgDTOo
    {
        public UspAoSiginFlgDTOo()
        {
            data = new Data();
        }

        public String error_code { get; set; }
        public String error_msg { get; set; }
        public Data data;

        public class Data
        {
            public String loginResult { get; set; }
        }
    }

    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class UspAoSiginFlgController : ApiController
    {
        // POST api/<controller>
        public UspAoSiginFlgDTOo Post(UspAoSiginFlgDTI UspAoSiginFlgDTI)
        {
            UspAoSiginFlgDTOo UspAoSiginFlgDTOo = new UspAoSiginFlgDTOo();

            try
            {
                string result = new HttpPostClient().exec(WebConfigurationManager.AppSettings["server2_ip"].ToString() + "api/USP_AO_SIGIN_FLG", UspAoSiginFlgDTI);

                var jsonFrom = new JavaScriptSerializer();
                UspAoSiginFlgDTO UspAoSiginFlgDTO = jsonFrom.Deserialize<UspAoSiginFlgDTO>(result);
                //if (UspAoSiginFlgDTO.ERROR_CODE == ErrorInfo.OK_CODE)
                //{
                    UspAoSiginFlgDTOo.data = new UspAoSiginFlgDTOo.Data();
                    foreach (UspAoSiginFlgDTO.Data _data in UspAoSiginFlgDTO.data)
                    {

                        UspAoSiginFlgDTOo.data.loginResult = _data.LOGIN_RESULT;

                    }
                //}
                //---------------------------
                //      Message
                //---------------------------
                UspAoSiginFlgDTOo.error_code = UspAoSiginFlgDTO.ERROR_CODE;
                UspAoSiginFlgDTOo.error_msg = UspAoSiginFlgDTO.ERROR_MSG;
            }
            catch (Exception ex)
            {
                UspAoSiginFlgDTOo.error_code = ErrorInfo.CATCH_CODE;
                UspAoSiginFlgDTOo.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }

            return UspAoSiginFlgDTOo;
        }

    }
}