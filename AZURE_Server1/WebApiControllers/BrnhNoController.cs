﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/BrnhNo



    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class BrnhNoDTI : LoginDTI
    {
        /*
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String MOBILE_TYPE { get; set; }
        public String OS_NAME { get; set; }
        public String OS_VERNO { get; set; }
        */
        public String DLR_NO { get; set; }
    }

    public class BrnhNoDTO
    {
        public BrnhNoDTO()
        {
            data = new List<Data>();
        }
        public String error_code { get; set; }
        public String error_msg { get; set; }
        public List<Data> data;
        public class Data
        {
            public String dlrNo { get; set; }
            public String brnhNo { get; set; }
            public String brnhNoNm { get; set; }
            public String philoNo { get; set; }
        }
    }


    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class BrnhNoController : ApiController
    {

        // POST api/<controller>
        public BrnhNoDTO Post(BrnhNoDTI brnhNoDTI)
        {
            LoginDTOo loginDTOo = new LoginController().Post(brnhNoDTI);

            BrnhNoDTO brnhNoDTO = new BrnhNoDTO();
            if (loginDTOo.data.loginResult == "Y")
            {
                brnhNoDTO = get(brnhNoDTI);
                brnhNoDTO.error_code = ErrorInfo.OK_CODE;
                brnhNoDTO.error_msg = ErrorInfo.OK_MSG;
            }
            else
            {
                brnhNoDTO.error_code = ErrorInfo.IDNG_CODE;
                brnhNoDTO.error_msg = ErrorInfo.IDNG_MSG;
            }
            return brnhNoDTO;
        }

        private BrnhNoDTO get(BrnhNoDTI brnhNoDTI)
        {
            BrnhNoDTO brnhNoDTO = new BrnhNoDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["AE_DB"].ToString();

                SqlConnection ConnMSSQL = new SqlConnection(connectionString);
                ConnMSSQL.Open();

                String cmdStr = @"SELECT TOP(1000) 
                                                [DLR_NO], [BRNH_NO],[BRNH_NO_NM],[PHILO_NO]
                                                FROM[AE].[dbo].[AE_BRNH_NO]";

                if(brnhNoDTI.DLR_NO!=null && brnhNoDTI.DLR_NO != "")
                {
                    cmdStr += @" WHERE DLR_NO like @dlrNo;";
                }
                else
                {
                    cmdStr += @";";
                }


                SqlCommand cmd = new SqlCommand(cmdStr, ConnMSSQL);

                if (brnhNoDTI.DLR_NO != null && brnhNoDTI.DLR_NO != "")
                {
                    cmd.Parameters.AddWithValue("@dlrNo", brnhNoDTI.DLR_NO);
                }

                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        BrnhNoDTO.Data data = new BrnhNoDTO.Data();
                        data.dlrNo = dr["DLR_NO"].ToString();
                        data.brnhNo = dr["BRNH_NO"].ToString();
                        data.brnhNoNm = dr["BRNH_NO_NM"].ToString();
                        data.philoNo = dr["PHILO_NO"].ToString();
                        brnhNoDTO.data.Add(data);
                        
                    }
                }

                //------------------
                //  關閉資料庫連線
                //------------------

                ConnMSSQL.Close();
                ConnMSSQL.Dispose();

                brnhNoDTO.error_code = ErrorInfo.OK_CODE;
                brnhNoDTO.error_msg = ErrorInfo.OK_MSG;
            }
            catch (Exception ex)
            {
                brnhNoDTO.error_code = ErrorInfo.CATCH_CODE;
                brnhNoDTO.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }



            return brnhNoDTO;

        }


    }
}