﻿using Config;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[assembly: OwinStartup(typeof(AppsCore.Startup))]

namespace AppsCore
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = WebApiConfig.GetConfig();
            app.UseWebApi(config);
        }
    }
}