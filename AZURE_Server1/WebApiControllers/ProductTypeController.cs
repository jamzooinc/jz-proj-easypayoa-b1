﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace AZURE_Server1
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/ProductType



    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class ProductTypeDTI : LoginDTI
    {
        /*
        public String SALES_NO { get; set; }
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String MOBILE_TYPE { get; set; }
        public String OS_NAME { get; set; }
        public String OS_VERNO { get; set; }
        */
    }

    public class ProductTypeDTO
    {
        public ProductTypeDTO()
        {
            data = new List<Data>();
        }
        public String error_code { get; set; }
        public String error_msg { get; set; }
        public List<Data> data;
        public class Data
        {
            public String prodType { get; set; }
            public String prodTypeNm { get; set; }
        }
    }


    //*********************************************************************
    //*********************************************************************
    //      
    //=====================================================================
    public class ProductTypeController : ApiController
    {

        // POST api/<controller>
        public ProductTypeDTO Post(ProductTypeDTI productTypeDTI)
        {
            LoginDTOo loginDTOo = new LoginController().Post(productTypeDTI);

            ProductTypeDTO productTypeDTO = new ProductTypeDTO();
            if (loginDTOo.data.loginResult == "Y")
            {
                productTypeDTO = get();
                productTypeDTO.error_code = ErrorInfo.OK_CODE;
                productTypeDTO.error_msg = ErrorInfo.OK_MSG;
            }
            else
            {
                productTypeDTO.error_code = ErrorInfo.IDNG_CODE;
                productTypeDTO.error_msg = ErrorInfo.IDNG_MSG;
            }
            return productTypeDTO;
        }


        private ProductTypeDTO get()
        {
            ProductTypeDTO productTypeDTO = new ProductTypeDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["AE_DB"].ToString();

                SqlConnection ConnMSSQL = new SqlConnection(connectionString);
                ConnMSSQL.Open();

                String testStoreProcedureStr = @"SELECT * FROM AE_PROD_TYPE;";
                SqlCommand cmd = new SqlCommand(testStoreProcedureStr, ConnMSSQL);
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ProductTypeDTO.Data data = new ProductTypeDTO.Data();
                        data.prodType = dr["PROD_TYPE"].ToString();
                        data.prodTypeNm = dr["PROD_TYPE_NM"].ToString();
                        productTypeDTO.data.Add(data);
                    }
                }

                //------------------
                //  關閉資料庫連線
                //------------------

                ConnMSSQL.Close();
                ConnMSSQL.Dispose();

                productTypeDTO.error_code = ErrorInfo.OK_CODE;
                productTypeDTO.error_msg = ErrorInfo.OK_MSG;
            }
            catch (Exception ex)
            {
                productTypeDTO.error_code = ErrorInfo.CATCH_CODE;
                productTypeDTO.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }



            return productTypeDTO;

        }

    }
}