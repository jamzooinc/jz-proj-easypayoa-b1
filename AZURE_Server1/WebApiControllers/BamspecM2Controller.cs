﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/BamspecM2

    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class BamspecMTwoDTI
    {
        public String PROD_TYPE { get; set; }
        public String FUND_SRC { get; set; }
        public String SPEC_NAME1 { get; set; }
        public String SPEC_NAME2 { get; set; }
        public String SALES_NO { get; set; }
    }

    public class BamspecMTwoDTO
    {
        public BamspecMTwoDTO()
        {
            data = new List<Data>();
        }

        public String error_code { get; set; }
        public String error_msg { get; set; }

        public String loginResult { get; set; }
        public String errMsg { get; set; }

        public List<Data> data;

        public class Data
        {
            public String specNo { get; set; }
            public String specNoNm { get; set; }
        }
    }

    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================
    public class BamspecM2Controller : ApiController
    {


        // POST api/<controller>
        public BamspecMTwoDTO Post(BamspecMTwoDTI bamspecMTwoDTI)
        {
            return get(bamspecMTwoDTI);
        }


        private BamspecMTwoDTO get(BamspecMTwoDTI bamspecMTwoDTI)
        {
            BamspecMTwoDTO bamspecMTwoDTO = new BamspecMTwoDTO();

            try
            {
                string connectionString = WebConfigurationManager.AppSettings["AE_DB"].ToString();

                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(string.Empty, conn);
                conn.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure;
                if (!string.IsNullOrEmpty(bamspecMTwoDTI.SALES_NO))
                {
                    cmd.CommandText = "USP_AO_APP_BAMSPEC_M_ID";
                    cmd.Parameters.Add("@SALES_ID", SqlDbType.VarChar).Value = bamspecMTwoDTI.SALES_NO;
                }
                else
                {
                    cmd.CommandText = "USP_AO_APP_BAMSPEC_M";
                }
                cmd.Parameters.Add("@PROD_TYPE", SqlDbType.VarChar).Value = bamspecMTwoDTI.PROD_TYPE;
                cmd.Parameters.Add("@FUND_SRC", SqlDbType.VarChar).Value = bamspecMTwoDTI.FUND_SRC;
                cmd.Parameters.Add("@SPEC_NAME1", SqlDbType.VarChar).Value = bamspecMTwoDTI.SPEC_NAME1;
                cmd.Parameters.Add("@SPEC_NAME2", SqlDbType.VarChar).Value = bamspecMTwoDTI.SPEC_NAME2;
                

                SqlDataReader reader = cmd.ExecuteReader(); //CommandBehavior.SingleRow

                if (reader.HasRows)
                {
                    //int cnt = 0;
                    while (reader.Read())
                    {
                        BamspecMTwoDTO.Data data = new BamspecMTwoDTO.Data();

                        bamspecMTwoDTO.loginResult = reader["LOGIN_RESULT"].ToString();
                        bamspecMTwoDTO.errMsg = reader["ERR_MSG"].ToString();

                        data.specNo = reader["SPEC_NO"].ToString();
                        data.specNoNm = reader["SPEC_NO_NM"].ToString();

                      //  if (cnt != 50)
                     //   {
                      //      cnt++;
                            bamspecMTwoDTO.data.Add(data);
                     //   }
                    }
                }

                reader.Close();
                reader.Dispose();

                conn.Close();
                conn.Dispose();
                //---------------------------
                //      Message
                //---------------------------
                bamspecMTwoDTO.error_code = ErrorInfo.OK_CODE;
                bamspecMTwoDTO.error_msg = ErrorInfo.OK_MSG;

            }
            catch (Exception ex)
            {
                bamspecMTwoDTO.error_code = ErrorInfo.CATCH_CODE;
                bamspecMTwoDTO.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }



            return bamspecMTwoDTO;
        }
    }
}