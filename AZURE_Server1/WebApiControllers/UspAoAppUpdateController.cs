﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AZURE_Server1.WebApiControllers
{
    //*********************************************************************
    //*********************************************************************
    //      ip
    //=====================================================================
    //http://localhost:55015/api/UspAoAppUpdate


    //*********************************************************************
    //*********************************************************************
    //      DTI(data to in)   and   DTO(data to out)   class
    //=====================================================================
    public class UspAoAppUpdateDTI
    {
        public String MOBILE_NO { get; set; }
        public String MOBILE_ID { get; set; }
        public String APP_CASE_NO { get; set; }
    }

    public class UspAoAppUpdateDTO
    {
        public UspAoAppUpdateDTO()
        {
            data = new Data();
        }

        public String ERROR_CODE { get; set; }
        public String ERROR_MSG { get; set; }
        public Data data;

        public class Data
        {
            public String LOGIN_RESULT { get; set; }
            public String ERR_MSG { get; set; }
            public String ERR_NO { get; set; }
            public String APP_CASE_NO { get; set; }
            public String APPL_NO { get; set; }
            public String P_ID { get; set; }
            public String CH_NAME { get; set; }
            public String CASE_STATUS { get; set; }
            public String UPDATE_FLG { get; set; }
            //public String APP_FORM_2 { get; set; }//
            public String PROD_TYPE { get; set; }
            public String PROD_TYPE_NM { get; set; }
            public String FUND_SRC { get; set; }
            public String FUND_SRC_NM { get; set; }
            public String BANK_NA { get; set; }
            public String BANK_NA_NM { get; set; }
            //public String NOQRY_FLG { get; set; }//
            public String PROJ_ID { get; set; }
            public String PROJ_ID_NM { get; set; }
            public String DLR_NO { get; set; }
            public String DLR_NO_NM { get; set; }
            public String BRNH_NO { get; set; }
            public String BRNH_NO_NM { get; set; }
            public String SALES_NO { get; set; }
            public String SALES_NO_NM { get; set; }
            public String MSG_CNT { get; set; }
            public String SEQ_NO { get; set; }
            public String BACK_MENT { get; set; }
        }
    }


    public class UspAoAppUpdateDTOo
    {
        public UspAoAppUpdateDTOo()
        {
            data = new Data();
        }

        public String error_code { get; set; }
        public String error_msg { get; set; }
        public Data data;

        public class Data
        {
            public String loginResult { get; set; }
            public String errMsg { get; set; }
            public String errNo { get; set; }
            public String appCaseNo { get; set; }
            public String appNo { get; set; }
            public String pID { get; set; }
            public String chName { get; set; }
            public String caseStatus { get; set; }
            public String updateFlg { get; set; }
            //public String appForm2 { get; set; }  //
            public String prosType { get; set; }
            public String prodTypeNm { get; set; }
            public String fundSrc { get; set; }
            public String fundSrcNm { get; set; }
            public String bankNa { get; set; }
            public String bankNaNm { get; set; }
            //public String NoqryFlg { get; set; }//
            public String projID { get; set; }
            public String projIdNm { get; set; }
            public String dlrNo { get; set; }
            public String dlrNoNm { get; set; }
            public String brnhNo { get; set; }
            public String brohNoNm { get; set; }
            public String salesNo { get; set; }
            public String salesNoNm { get; set; }
            public String msgCnt { get; set; }
            public String seqNo { get; set; }
            public String backMent { get; set; }
        }
    }
    //*********************************************************************
    //*********************************************************************
    //
    //=====================================================================
    public class UspAoAppUpdateController : ApiController
    {


        // POST api/<controller>
        public UspAoAppUpdateDTOo Post(UspAoAppUpdateDTI uspAoAppUpdateDTI)
        {
            UspAoAppUpdateDTOo uspAoAppUpdateDTOo = new UspAoAppUpdateDTOo();

            try
            {
                string result = new HttpPostClient().exec(WebConfigurationManager.AppSettings["server2_ip"].ToString() + "api/USP_AO_APP_UPDATE", uspAoAppUpdateDTI);

                var jsonFrom = new JavaScriptSerializer();
                UspAoAppUpdateDTO uspAoAppUpdateDTO = jsonFrom.Deserialize<UspAoAppUpdateDTO>(result);
                if (uspAoAppUpdateDTO.ERROR_CODE == "200")
                {
                    uspAoAppUpdateDTOo.data.loginResult = uspAoAppUpdateDTO.data.LOGIN_RESULT;
                    uspAoAppUpdateDTOo.data.errMsg = uspAoAppUpdateDTO.data.ERR_MSG;
                    uspAoAppUpdateDTOo.data.errNo = uspAoAppUpdateDTO.data.ERR_NO;
                    uspAoAppUpdateDTOo.data.appCaseNo = uspAoAppUpdateDTO.data.APP_CASE_NO;
                    uspAoAppUpdateDTOo.data.appNo = uspAoAppUpdateDTO.data.APPL_NO;
                    uspAoAppUpdateDTOo.data.pID = uspAoAppUpdateDTO.data.P_ID;
                    uspAoAppUpdateDTOo.data.chName = uspAoAppUpdateDTO.data.CH_NAME;
                    uspAoAppUpdateDTOo.data.caseStatus = uspAoAppUpdateDTO.data.CASE_STATUS;
                    uspAoAppUpdateDTOo.data.updateFlg = uspAoAppUpdateDTO.data.UPDATE_FLG;
                    //uspAoAppUpdateDTOo.data.appForm2 = uspAoAppUpdateDTO.data.APP_FORM_2;//
                    uspAoAppUpdateDTOo.data.prosType = uspAoAppUpdateDTO.data.PROD_TYPE;
                    uspAoAppUpdateDTOo.data.prodTypeNm = uspAoAppUpdateDTO.data.PROD_TYPE_NM;
                    uspAoAppUpdateDTOo.data.fundSrc = uspAoAppUpdateDTO.data.FUND_SRC;
                    uspAoAppUpdateDTOo.data.fundSrcNm = uspAoAppUpdateDTO.data.FUND_SRC_NM;
                    uspAoAppUpdateDTOo.data.bankNa = uspAoAppUpdateDTO.data.BANK_NA;
                    uspAoAppUpdateDTOo.data.bankNaNm = uspAoAppUpdateDTO.data.BANK_NA_NM;
                    //uspAoAppUpdateDTOo.data.NoqryFlg = uspAoAppUpdateDTO.data.NOQRY_FLG;//
                    uspAoAppUpdateDTOo.data.projID = uspAoAppUpdateDTO.data.PROJ_ID;
                    uspAoAppUpdateDTOo.data.projIdNm = uspAoAppUpdateDTO.data.PROJ_ID_NM;
                    uspAoAppUpdateDTOo.data.dlrNo = uspAoAppUpdateDTO.data.DLR_NO;
                    uspAoAppUpdateDTOo.data.dlrNoNm = uspAoAppUpdateDTO.data.DLR_NO_NM;
                    uspAoAppUpdateDTOo.data.brnhNo = uspAoAppUpdateDTO.data.BRNH_NO;
                    uspAoAppUpdateDTOo.data.brohNoNm = uspAoAppUpdateDTO.data.BRNH_NO_NM;
                    uspAoAppUpdateDTOo.data.salesNo = uspAoAppUpdateDTO.data.SALES_NO;
                    uspAoAppUpdateDTOo.data.salesNoNm = uspAoAppUpdateDTO.data.SALES_NO_NM;
                    uspAoAppUpdateDTOo.data.msgCnt = uspAoAppUpdateDTO.data.MSG_CNT;
                    uspAoAppUpdateDTOo.data.seqNo = uspAoAppUpdateDTO.data.SEQ_NO;
                    uspAoAppUpdateDTOo.data.backMent = uspAoAppUpdateDTO.data.BACK_MENT;
                }
                //---------------------------
                //      Message
                //---------------------------
                uspAoAppUpdateDTOo.error_code = uspAoAppUpdateDTO.ERROR_CODE;
                uspAoAppUpdateDTOo.error_msg = uspAoAppUpdateDTO.ERROR_MSG;
            }
            catch (Exception ex)
            {
                uspAoAppUpdateDTOo.error_code = ErrorInfo.CATCH_CODE;
                uspAoAppUpdateDTOo.error_msg = ErrorInfo.CATCH_MSG + ex.ToString();
            }

            return uspAoAppUpdateDTOo;
        }


    }
}